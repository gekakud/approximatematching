﻿/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace Fast_Approximate_Matching_GUI_Server
{
    public partial class ServerForm : Form
    {
        private string db;
        public const int SEQ_SIZE = 20;
        Process exeProcess;
        bool ready = false;
        string filename = "ProteinDB2.txt";
        string path = "E:\\";
        StringBuilder outputBuilder = new StringBuilder();
        string output = "";
        bool finished = false;
        private MatchLocations[] matches;
        string sequence = "";

        private void CreateServer()
        {
            exeProcess.BeginOutputReadLine();
            var tcp = new TcpListener(IPAddress.Any, 25265);
            tcp.Start();

            var listeningThread = new Thread(() =>
            {
                while (true)
                {
                    while (!ready) { }
                    var tcpClient = tcp.AcceptTcpClient();
                    ThreadPool.QueueUserWorkItem(param =>
                    {
                        NetworkStream stream = tcpClient.GetStream();
                        byte[] bytes = new byte[1024];
                        int i = stream.Read(bytes, 0, bytes.Length);
                        string incoming = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        sequence = incoming.Substring(0, SEQ_SIZE);
                        StringReader reader = new StringReader(incoming.Substring(SEQ_SIZE));
                        string[] stringSeparators2 = new string[] { " " };
                        string[] positionsStrings = reader.ReadToEnd().Split(stringSeparators2, StringSplitOptions.RemoveEmptyEntries);
                        int mismatches = int.Parse(positionsStrings[0]);
                        console.Invoke(
                        (MethodInvoker)delegate
                        {
                            console.Text += String.Format("{0} Connection esatblished: {1}{2}", DateTime.Now, tcpClient.Client.RemoteEndPoint, Environment.NewLine);
                        });

                        //MessageBox.Show(String.Format("Received: {0}", incomming));
                        incommingMessages.Invoke((MethodInvoker)(() => incommingMessages.Items.Add(sequence)));
                        FindSequenceInText(sequence + " " + mismatches);
                        while (!finished) { }
                        Byte[] data = System.Text.Encoding.ASCII.GetBytes(output);
                        stream.Write(data, 0, data.Length);
                        outputBuilder = new StringBuilder();
                        output = "";
                        sequence = "";
                        finished = false;
                        tcpClient.Close();           
                    }, null);
                }
            });

            listeningThread.IsBackground = true;
            listeningThread.Start();
        }

        private void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (!ready && (outLine.Data == "DB read started" || outLine.Data == "DB read finished" || outLine.Data == "Preprocess started"))
                console.Invoke(new Action(() => console.Text += outLine.Data.ToString() + "\r\n"));
            else if (!ready && outLine.Data == "Preprocess finished")
            {
                console.Invoke(new Action(() => console.Text += outLine.Data.ToString() + "\r\n"));
                exeProcess.CancelOutputRead();
                ready = true;
            }
            else if (ready && outLine.Data == "end")
            {
                exeProcess.CancelOutputRead();
                output = outputBuilder.ToString();
                getPositions();
            }
            else if (ready && !String.IsNullOrEmpty(outLine.Data) && outputBuilder.ToString() == "")
                outputBuilder.Append(outLine.Data);
            else if (ready && !String.IsNullOrEmpty(outLine.Data))
                outputBuilder.Append("\r\n" + outLine.Data);
        }

        private void FindSequenceInText(string sequence)
        {
            StreamWriter myStreamWriter = exeProcess.StandardInput;
            myStreamWriter.WriteLine(sequence);
            exeProcess.BeginOutputReadLine();
        }

        public ServerForm()
        {
            InitializeComponent();
            this.FormClosed += OnFormClosed;
            HandleCreated += Server_HandleCreated;
        }

        private void Server_HandleCreated(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.FileName = "C:\\Users\\Ben Peretz\\Documents\\Visual Studio 2015\\Projects\\Fast Approximate Matching\\x64\\Release\\Fast Approximate Matching.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            string DBLocation = path + filename;
            int mismatches = 8;
            int form_size = 4;
            string arguments = DBLocation + " " + mismatches + " " + form_size;
            startInfo.Arguments = arguments;

            exeProcess = Process.Start(startInfo);
            exeProcess.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            CreateServer();
        }

        private void OnFormClosed(object sender, FormClosedEventArgs e)
        {
            StreamWriter myStreamWriter = exeProcess.StandardInput;
            myStreamWriter.WriteLine("q");
        }

        private void Server_Load(object sender, EventArgs e)
        {

        }

        private void getPositions()
        {
            StringReader reader = new StringReader(output);
            string[] stringSeparators1 = new string[] { "\r\n" };
            string[] lines = reader.ReadToEnd().Split(stringSeparators1, StringSplitOptions.RemoveEmptyEntries);
            string[] stringSeparators2 = new string[] { " " };

            matches = new MatchLocations[lines.Length / 4];
            int j = 0;

            for (int i = 0; i < lines.Length - 1; i += 4)
            {
                List<int> positionsInText = new List<int>();
                List<int> positionsInSequence = new List<int>();
                int proteinNumber = int.Parse(lines[i]);
                string proteinName = lines[i + 1];
                StringReader reader2 = new StringReader(lines[i + 2]);
                string[] positionsStrings = reader2.ReadToEnd().Split(stringSeparators2, StringSplitOptions.RemoveEmptyEntries);
                for (int k = 0; k < positionsStrings.Length; k++)
                    positionsInText.Add(int.Parse(positionsStrings[k]));
                reader2 = new StringReader(lines[i + 3]);
                positionsStrings = reader2.ReadToEnd().Split(stringSeparators2, StringSplitOptions.RemoveEmptyEntries);
                for (int k = 0; k < positionsStrings.Length; k++)
                    positionsInSequence.Add(int.Parse(positionsStrings[k]));

                matches[j] = new MatchLocations(proteinNumber, proteinName, positionsInText, positionsInSequence);
                j++;
            }
            string DBLocation2 = "New" + filename;
            using (var streamReader = new StreamReader(DBLocation2))
            {
                db = streamReader.ReadToEnd();
            }

            output += "\r\nEndPositions\r\n";
            for (int i = 0; i < lines.Length / 4; i++)
            {
                int firstPositionInText = matches[i].getFirstPositionInText();
                int firstPositionInSequence = matches[i].getFirstPositionInSequence();
                for (int b = 0; b < SEQ_SIZE; b++)
                    output += db[firstPositionInText - firstPositionInSequence + b] + " ";
                output += "\r\n";
            }

            finished = true;
        }
    }
}
