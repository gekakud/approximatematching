﻿/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace Fast_Approximate_Matching_GUI_Server
{
    public partial class ServerForm : Form
    {
        public const int SEQ_SIZE = 20;
        Process exeProcess;
        bool ready = false;
        string filename = "E:\\ProteinDB4.txt";
        StringBuilder outputBuilder = new StringBuilder();
        string output = "";
        bool finished = false;
        string sequence = "";
        Int32 port = 25265;
        Thread oThread;

        private string GetExternalIPAddress()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("https://api.ipify.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch
            {
                return null;
            }
        }

        private void getNetworkDetails()
        {
            tbPort.Invoke(new Action(() => tbPort.Text = port.ToString()));
            tbPort.Invoke(new Action(() => tbPort.Refresh()));
            string ipAddress = GetExternalIPAddress();
            tbIPAddress.Invoke(new Action(() => tbIPAddress.Text = ipAddress));
            tbIPAddress.Invoke(new Action(() => tbIPAddress.Refresh())); 
        }

        private void CreateServer()
        {
            exeProcess.BeginOutputReadLine();
            var tcp = new TcpListener(System.Net.IPAddress.Any, port);
            tcp.Start();

            var listeningThread = new Thread(() =>
            {
                while (true)
                {
                    while (!ready) { }
                    var tcpClient = tcp.AcceptTcpClient();
                    ThreadPool.QueueUserWorkItem(param =>
                    {
                        NetworkStream stream = tcpClient.GetStream();
                        byte[] bytes = new byte[1024];
                        int i = stream.Read(bytes, 0, bytes.Length);
                        string incoming = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        sequence = incoming.Substring(0, SEQ_SIZE);
                        StringReader reader = new StringReader(incoming.Substring(SEQ_SIZE));
                        string[] stringSeparators2 = new string[] { " " };
                        string[] positionsStrings = reader.ReadToEnd().Split(stringSeparators2, StringSplitOptions.RemoveEmptyEntries);
                        string isSimilarityFlag = positionsStrings[0];
                        int mismatches = 0;
                        if (!(String.Compare(isSimilarityFlag, "similarityGraph") == 0))
                        {
                            mismatches = int.Parse(positionsStrings[0]);
                            incommingMessages.Invoke((MethodInvoker)(() => incommingMessages.Items.Add(sequence + " " + mismatches)));
                        }
                        else
                            incommingMessages.Invoke((MethodInvoker)(() => incommingMessages.Items.Add("Similarity graph")));
                        console.Invoke(
                        (MethodInvoker)delegate
                        {
                            console.Text += String.Format("{0} Connection esatblished: {1}{2}", DateTime.Now, tcpClient.Client.RemoteEndPoint, Environment.NewLine);
                        });

                        if (!(String.Compare(isSimilarityFlag, "similarityGraph") == 0))
                            FindSequenceInText(sequence + " " + mismatches);
                        else
                            GetSimilarityGraph();
                        while (!finished) { }
                        Byte[] data = System.Text.Encoding.ASCII.GetBytes(output);
                        stream.Write(data, 0, data.Length);
                        outputBuilder = new StringBuilder();
                        output = "";
                        sequence = "";
                        finished = false;
                        tcpClient.Close();           
                    }, null);
                }
            });

            listeningThread.IsBackground = true;
            listeningThread.Start();
        }

        private void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (!ready && (outLine.Data == "DB read started" || outLine.Data == "DB read finished" || outLine.Data == "Preprocess started"))
                console.Invoke(new Action(() => console.Text += outLine.Data.ToString() + "\r\n"));
            else if (!ready && outLine.Data == "Preprocess finished")
            {
                console.Invoke(new Action(() => console.Text += outLine.Data.ToString() + "\r\n"));
                exeProcess.CancelOutputRead();
                ready = true;
            }
            else if (ready && outLine.Data == "end")
            {
                exeProcess.CancelOutputRead();
                output = outputBuilder.ToString() + "\r\nend";
                finished = true;
            }
            else if (ready && !String.IsNullOrEmpty(outLine.Data) && outputBuilder.ToString() == "")
                outputBuilder.Append(outLine.Data);
            else if (ready && !String.IsNullOrEmpty(outLine.Data))
                outputBuilder.Append("\r\n" + outLine.Data);
        }

        private void FindSequenceInText(string sequence)
        {
            StreamWriter myStreamWriter = exeProcess.StandardInput;
            myStreamWriter.WriteLine(sequence);
            exeProcess.BeginOutputReadLine();
        }

        public ServerForm()
        {
            InitializeComponent();
            this.FormClosed += OnFormClosed;
            HandleCreated += Server_HandleCreated;
        }

        private void Server_HandleCreated(object sender, EventArgs e)
        {
            oThread = new Thread(getNetworkDetails);
            oThread.Start();
        }

        public void GetSimilarityGraph()
        {
            String temp = Directory.GetCurrentDirectory();
            Environment.CurrentDirectory = Directory.GetCurrentDirectory() + "\\sg\\";
            Process exeProcess2 = new Process();
            exeProcess2.StartInfo.FileName = "sg.exe";
            exeProcess2.StartInfo.CreateNoWindow = true;
            exeProcess2.StartInfo.UseShellExecute = false;
            exeProcess2.Start();
            exeProcess2.WaitForExit();
            exeProcess2.Close();
            output = File.ReadAllText("clustring.txt");
            finished = true;

            Environment.CurrentDirectory = temp;
        }

        private void OnFormClosed(object sender, FormClosedEventArgs e)
        {
            if (exeProcess != null)
            {
                StreamWriter myStreamWriter = exeProcess.StandardInput;
                myStreamWriter.WriteLine("q");
            }
            oThread.Abort();
        }

        private void Server_Load(object sender, EventArgs e)
        {

        }

        private void bBrowse_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a Cursor.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Text files|*.txt";
            openFileDialog1.Title = "Select a DB";

            // Show the Dialog.
            // If the user clicked OK in the dialog and
            // a .txt file was selected, open it.
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                tbFilePath.Text = openFileDialog1.FileName;
                filename = openFileDialog1.FileName;
            }
        }

        private void bStartPreprocess_Click(object sender, EventArgs e)
        {
            bBrowse.Enabled = false;
            bStartPreprocess.Enabled = false;

            String temp = Directory.GetCurrentDirectory();
            Environment.CurrentDirectory = Directory.GetCurrentDirectory() + "\\fam\\";
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.FileName = "fam.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            int mismatches = 8;
            int form_size = 4;
            string arguments = filename + " " + mismatches + " " + form_size;
            startInfo.Arguments = arguments;

            exeProcess = Process.Start(startInfo);
            exeProcess.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            CreateServer();

            Environment.CurrentDirectory = temp;
        }
    }
}
