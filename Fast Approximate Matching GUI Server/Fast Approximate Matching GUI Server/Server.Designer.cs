﻿namespace Fast_Approximate_Matching_GUI_Server
{
    partial class ServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.incommingMessages = new System.Windows.Forms.ListBox();
            this.console = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbIPAddress = new System.Windows.Forms.TextBox();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bBrowse = new System.Windows.Forms.Button();
            this.tbFilePath = new System.Windows.Forms.TextBox();
            this.bStartPreprocess = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // incommingMessages
            // 
            this.incommingMessages.FormattingEnabled = true;
            this.incommingMessages.ItemHeight = 16;
            this.incommingMessages.Location = new System.Drawing.Point(68, 143);
            this.incommingMessages.Name = "incommingMessages";
            this.incommingMessages.Size = new System.Drawing.Size(988, 116);
            this.incommingMessages.TabIndex = 1;
            // 
            // console
            // 
            this.console.Location = new System.Drawing.Point(68, 332);
            this.console.Multiline = true;
            this.console.Name = "console";
            this.console.Size = new System.Drawing.Size(988, 290);
            this.console.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "IP Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Port:";
            // 
            // tbIPAddress
            // 
            this.tbIPAddress.Location = new System.Drawing.Point(195, 37);
            this.tbIPAddress.Name = "tbIPAddress";
            this.tbIPAddress.ReadOnly = true;
            this.tbIPAddress.Size = new System.Drawing.Size(243, 22);
            this.tbIPAddress.TabIndex = 5;
            this.tbIPAddress.Text = "Getting IP...";
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(195, 69);
            this.tbPort.Name = "tbPort";
            this.tbPort.ReadOnly = true;
            this.tbPort.Size = new System.Drawing.Size(243, 22);
            this.tbPort.TabIndex = 6;
            this.tbPort.Text = "Getting port...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(824, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Choose DB:";
            // 
            // bBrowse
            // 
            this.bBrowse.Location = new System.Drawing.Point(913, 15);
            this.bBrowse.Name = "bBrowse";
            this.bBrowse.Size = new System.Drawing.Size(100, 30);
            this.bBrowse.TabIndex = 8;
            this.bBrowse.Text = "Browse";
            this.bBrowse.UseVisualStyleBackColor = true;
            this.bBrowse.Click += new System.EventHandler(this.bBrowse_Click);
            // 
            // tbFilePath
            // 
            this.tbFilePath.Location = new System.Drawing.Point(809, 51);
            this.tbFilePath.Name = "tbFilePath";
            this.tbFilePath.ReadOnly = true;
            this.tbFilePath.Size = new System.Drawing.Size(204, 22);
            this.tbFilePath.TabIndex = 9;
            // 
            // bStartPreprocess
            // 
            this.bStartPreprocess.Location = new System.Drawing.Point(827, 79);
            this.bStartPreprocess.Name = "bStartPreprocess";
            this.bStartPreprocess.Size = new System.Drawing.Size(167, 37);
            this.bStartPreprocess.TabIndex = 10;
            this.bStartPreprocess.Text = "Start preprocess";
            this.bStartPreprocess.UseVisualStyleBackColor = true;
            this.bStartPreprocess.Click += new System.EventHandler(this.bStartPreprocess_Click);
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1129, 668);
            this.Controls.Add(this.bStartPreprocess);
            this.Controls.Add(this.tbFilePath);
            this.Controls.Add(this.bBrowse);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbPort);
            this.Controls.Add(this.tbIPAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.console);
            this.Controls.Add(this.incommingMessages);
            this.Name = "ServerForm";
            this.Text = "Fast Approximate Matching - Server";
            this.Load += new System.EventHandler(this.Server_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox incommingMessages;
        private System.Windows.Forms.TextBox console;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbIPAddress;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bBrowse;
        private System.Windows.Forms.TextBox tbFilePath;
        private System.Windows.Forms.Button bStartPreprocess;
    }
}

