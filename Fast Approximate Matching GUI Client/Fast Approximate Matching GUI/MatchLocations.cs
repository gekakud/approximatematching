﻿/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fast_Approximate_Matching_GUI_Client
{
    class MatchLocations
    {
        private int proteinNumber;
        private string proteinName;
        private int indexInProtein;
        private string sequenceInProtein;

        public MatchLocations()
        {

        }

        public MatchLocations(int proteinNumber, string proteinName, int indexInProtein, string sequenceInProtein)
        {
            this.indexInProtein = indexInProtein;
            this.sequenceInProtein = sequenceInProtein;
            this.proteinNumber = proteinNumber;
            this.proteinName = proteinName;
        }

        public void setIndexInProtein(int indexInProtein)
        {
            this.indexInProtein = indexInProtein;
        }

        public void setSequenceInProtein(string sequenceInProtein)
        {
            this.sequenceInProtein = sequenceInProtein;
        }

        public int getIndexInProtein()
        {
            return indexInProtein;
        }

        public string getSequenceInProtein()
        {
            return sequenceInProtein;
        }

        public int getProteinNumber()
        {
            return proteinNumber;
        }

        public string getProteinName()
        {
            return proteinName;
        }
    }
}
