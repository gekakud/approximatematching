﻿/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net.Sockets;

namespace Fast_Approximate_Matching_GUI_Client
{
    public partial class MainWindows : Form
    {
        private MatchLocations[] matches;
        public const int SEQ_SIZE = 20;

        void Connect(String server, String message, int flag)
        {
            try
            {
                Int32 port = 25265;
                TcpClient client = new TcpClient(server, port);
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                NetworkStream stream = client.GetStream();

                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, message.Length);

                while (true)
                {
                    if (stream.DataAvailable && flag == 0)
                    {
                        string incomming;
                        byte[] bytes = new byte[100000];
                        int i = stream.Read(bytes, 0, bytes.Length);
                        incomming = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        getPositions(incomming);
                        break;
                    }
                    else if (stream.DataAvailable && flag == 1)
                    {
                        string incomming;
                        byte[] bytes = new byte[100000];
                        int i = stream.Read(bytes, 0, bytes.Length);
                        incomming = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        System.IO.File.WriteAllText("SimilarityGraph.txt", incomming);
                        MessageBox.Show("Similarity graph was saved to the file: SimilarityGraph.txt");
                        similarityGraph.Enabled = true;
                        break;
                    }
                }

                stream.Close();
                client.Close();
                findSequence.Enabled = true;
            }
            catch (ArgumentNullException e)
            {
                Debug.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Debug.WriteLine("SocketException: {0}", e);
            }
        }

        public MainWindows()
        {
            InitializeComponent();
            MismatchesCombo.SelectedItem = "8";
        }

        private void getPositions(string output)
        {
            StringReader reader = new StringReader(output);
            string[] stringSeparators1 = new string[] { "\r\n" };
            string[] lines = reader.ReadToEnd().Split(stringSeparators1, StringSplitOptions.RemoveEmptyEntries);
            string[] stringSeparators2 = new string[] { " " };
            int i = 0;

            matches = new MatchLocations[lines.Length / 4];
            int j = 0;

            for (i = 0; i < lines.Length; i += 4)
            {
                if (String.Compare(lines[i], "end") == 0)
                    break;
                int proteinNumber = int.Parse(lines[i]);
                string proteinName = lines[i + 1];
                int indexInProtein = int.Parse(lines[i + 2]);
                string sequenceInProtein = lines[i + 3];

                matches[j] = new MatchLocations(proteinNumber, proteinName, indexInProtein, sequenceInProtein);
                j++;
            }

            PositionsList.Items.Clear();

            for (i = 0; i < matches.Length; i++)
                PositionsList.Items.Add(matches[i].getProteinNumber());

            TextLocationsLabel.Invoke(new Action(() => TextLocationsLabel.Enabled = true));
            ResultText.Invoke(new Action(() => ResultText.Enabled = true));
            PositionsList.Invoke(new Action(() => PositionsList.Enabled = true));

            TextLocationsLabel.Invoke(new Action(() => TextLocationsLabel.Refresh()));
            ResultText.Invoke(new Action(() => ResultText.Refresh()));
            PositionsList.Invoke(new Action(() => PositionsList.Refresh()));
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void findSequence_Click(object sender, EventArgs e)
        {
            if (SequenceText.Text.Length != SEQ_SIZE)
                MessageBox.Show("The size of the sequence is not " + SEQ_SIZE);
            else if (SequenceText.Text.Except("ARNDCEQGHILKMFPSTWYV").Any())
                MessageBox.Show("One or more of the characters is not an amino acid");
            else
            {
                findSequence.Enabled = false;
                Connect("localhost", SequenceText.Text + " " + MismatchesCombo.SelectedItem, 0);
                ResultText.Clear();
            }
        }

        private void PositionsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int positionIndex = PositionsList.SelectedIndex;
            if (positionIndex != -1)
            {
                ResultText.Text = "";
                ResultText.Text += "Protein number: " + matches[positionIndex].getProteinNumber() + "\r\n";
                ResultText.Text += "Protein name: " + matches[positionIndex].getProteinName() + "\r\n";
                ResultText.Text += "Index in protein: " + matches[positionIndex].getIndexInProtein() + "\r\n";
                ResultText.Text += "Sequence in protein: " + matches[positionIndex].getSequenceInProtein() + "\r\n";
            }
        }

        private void MainWindows_Load(object sender, EventArgs e)
        {

        }

        private void similarityGraph_Click(object sender, EventArgs e)
        {
            if (SequenceText.Text.Length != SEQ_SIZE)
                MessageBox.Show("The size of the sequence is not " + SEQ_SIZE);
            else if (SequenceText.Text.Except("ARNDCEQGHILKMFPSTWYV").Any())
                MessageBox.Show("One or more of the characters is not an amino acid");
            else
            {
                similarityGraph.Enabled = false;
                Connect("localhost", SequenceText.Text + " " + "similarityGraph", 1);
                ResultText.Clear();
            }
        }
    }
}
