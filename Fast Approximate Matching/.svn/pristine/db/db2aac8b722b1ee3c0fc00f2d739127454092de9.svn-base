/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "FindSequence.h"

//Find sequence in a text
void FindSequence(vector<string> &proteins, vector<vector<unsigned int>> &positions, vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, string &sequence, int form_size, int mismatches, int seqSize)
{
	unordered_map<string, Form*> sequenceForms;
	//clock_t begin, end;

	GetFormsSequence(sequenceForms, formTypes, sequence);

	unordered_map<string, Form*>::iterator it = sequenceForms.begin();
	map<unsigned char, vector<formPositions>>::iterator it3, iter3;
	vector<vector<unsigned int>>::iterator it4, it5;
	vector<vector<unsigned int>> possiblePositionsInText, possiblePositionsInSequence, realPositionsInText;
	int i, j;
	//Saving positions that were already checked for a match in an array for a complexity of O(1)

	//Hash table, the first key is the protein index and the second key is the position in the protein
	unordered_map<unsigned int, unordered_map<unsigned int, int>> positionsToCheck;
	unordered_map<unsigned int, unordered_map<unsigned int, int>>::iterator protIter;
	unordered_map<unsigned int, int>::iterator posIter;
	string formName;
	int index1, index2, index3, index4;

	//begin = clock();
	while (it != sequenceForms.end())
	{
		formName = (*it).first;
		index1 = AminoToIndexHash(formName[0]);
		index2 = AminoToIndexHash(formName[1]);
		index3 = AminoToIndexHash(formName[2]);
		index4 = AminoToIndexHash(formName[3]);
		if (forms[index1][index2][index3][index4] != NULL)
		{
			map<unsigned char, vector<formPositions>>* positionsInText = forms[index1][index2][index3][index4]->getPositions();
			map<unsigned char, vector<formPositions>>* positionsInSequence = (*it).second->getPositions();

			//Delete formTypes in text that don't exist in sequence
			it3 = (*positionsInText).begin();
			while (it3 != (*positionsInText).end())
			{
				if ((*positionsInSequence).find((*it3).first) == (*positionsInSequence).end())
				{
					unsigned char posToDelete = (*it3).first;
					it3++;
					(*positionsInText).erase(posToDelete);
				}
				else
					it3++;
			}

			//Delete formTypes in sequence that don't exist in text
			it3 = (*positionsInSequence).begin();
			while (it3 != (*positionsInSequence).end())
			{
				if ((*positionsInText).find((*it3).first) == (*positionsInText).end())
				{
					unsigned char posToDelete = (*it3).first;
					it3++;
					(*positionsInSequence).erase(posToDelete);
				}
				else
					it3++;
			}

			//For every position in text, duplicate position in sequence
			it3 = (*positionsInSequence).begin();
			while (it3 != (*positionsInSequence).end())
			{
				iter3 = (*positionsInText).find((*it3).first);
				if (iter3 != (*positionsInText).end())
				{
					for (i = 0; i < (*iter3).second.size() - 1; i++)
					{
						(*it).second->addPosition((*it3).first, 0, (*it3).second[i].positionInProtein);
					}
				}
				it3++;
			}

			//Add possible positions for a match in text and sequence
			iter3 = (*positionsInSequence).begin();
			it3 = (*positionsInText).begin();
			while (iter3 != (*positionsInSequence).end())
			{
				for (i = 0; i < (*iter3).second.size(); i++)
				{
					protIter = positionsToCheck.find((*it3).second[i].proteinIndex);
					if (protIter != positionsToCheck.end())
						posIter = (*protIter).second.find((*it3).second[i].positionInProtein - (*iter3).second[i].positionInProtein);
					if (protIter == positionsToCheck.end() || posIter == (*protIter).second.end())
					{
						unordered_map<unsigned int, int> newPosition;
						pair<unsigned int, int> tempPair = make_pair((*it3).second[i].positionInProtein - (*iter3).second[i].positionInProtein, 1);
						newPosition.insert(tempPair);
						pair<unsigned int, unordered_map<unsigned int, int>> tempPair2 = make_pair((*it3).second[i].proteinIndex - 0, newPosition);
						positionsToCheck.insert(tempPair2);
					}
					else if ((*posIter).second >= ceil(((seqSize - mismatches) / double(form_size)) - 1))
					{
						vector<unsigned int> possibleTextPosition;
						vector<unsigned int> possibleSeqPosition;
						possibleTextPosition.push_back((*it3).second[i].proteinIndex);
						possibleTextPosition.push_back((*it3).second[i].positionInProtein);
						possibleSeqPosition.push_back((*iter3).second[i].proteinIndex);
						possibleSeqPosition.push_back((*iter3).second[i].positionInProtein);
						possiblePositionsInText.push_back(possibleTextPosition);
						possiblePositionsInSequence.push_back(possibleSeqPosition);
						(*posIter).second = -1;
					}
					else if ((*posIter).second != -1)
						(*posIter).second++;
				}
				it3++;
				iter3++;
			}
		}
		it++;
	}
	//end = clock();
	//cout << "Time to find possible matches: " << getTime(begin, end) << endl;

	//begin = clock();

	it4 = possiblePositionsInSequence.begin();
	it5 = possiblePositionsInText.begin();
	int matchCounter;

	//For every possible position, check if there is a k-mismatch in that position
	//begin = clock();
	while (it4 != possiblePositionsInSequence.end())
	{
		int positionIndex = (*it5)[1] - (*it4)[1];
		matchCounter = 0;
		for (int i = 0; i < sequence.size(); i++)
		{
			if ((proteins[(*it5)[0]])[positionIndex + i] == sequence[i])
			{
				matchCounter++;
				if (matchCounter >= seqSize - mismatches)
					break;
			}
		}
		if (matchCounter >= seqSize - mismatches)
		{
			vector<unsigned int> newPosition;
			newPosition.push_back((*it5)[0]);
			newPosition.push_back((*it5)[1] - (*it4)[1]);
			positions.push_back(newPosition);
		}
		it4++;
		it5++;
	}
	//end = clock();
	//cout << "Time to find matches using the possible matches vector: " << getTime(begin, end) << endl;
}