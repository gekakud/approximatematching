/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "Preprocess.h"

bool PreprocessClass::CheckIfPreProcessed(int form_size, int mismatches, int seqSize)
{
	stringstream filename;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	ifstream myfile(filename.str());
	return myfile.good();
}

void PreprocessClass::InitializeHash()
{
	int i, j;
	for (i = 0; i < 255; i++)
		AATabQuick[i] = 20;

	for (i = 0; i < 255; i++)
		for (j = 0; j < 20; j++)
			if (AAcode[j] == i)
				AATabQuick[i] = j;
}

void PreprocessClass::changeAminoIndex(char changeFrom, char changeTo)
{
	AATabQuick[changeFrom] = AATabQuick[changeTo];
}

void PreprocessClass::GetForms(unordered_map<string, Form*> &forms, vector<FormType> &formTypes, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize)
{
	mutex mutex;
	//Loop through all form types
	for (int j = 0; j < formTypes.size(); j++)
		//parallel_for(size_t(0), formTypes.size(), [&](size_t j)
	{
		string formSeq;
		int distance;
		unordered_map<string, Form*>::iterator it;
		unsigned int i, limit;
		int k;
		//Create the forms with their position in the text according to the proteins positions
		bool skip = false;
		multimap<int, ProteinPosition>::iterator it2 = proteinsPositions.begin();
		while (it2 != proteinsPositions.end())
		{
			if (((*it2).second.endIndex - (*it2).second.startIndex) < formTypes[j].formType.size())
			{
				//cout << protein << endl;
				it2++;
				continue;
			}
			limit = ((*it2).second.endIndex - formTypes[j].formType.size());
			for (i = (*it2).second.startIndex; i <= limit; i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash(db[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += db[i + distance];
				}
				if (!skip)
				{
					//Check if the form already exists in the form list, if not then create a new form, else add its appearence position in the text
					it = forms.find(formSeq);
					if (it == forms.end())
					{
						Form *newForm = new Form(formTypes.size(), j, i);
						pair<string, Form*> tempPair = make_pair(formSeq, newForm);
						mutex.lock();
						forms.insert(tempPair);
						mutex.unlock();
					}
					else
					{
						mutex.lock();
						(*it).second->addPosition(j, i);
						mutex.unlock();
					}
				}
				skip = false;
			}
			it2++;
		}
		//});
	}
}

void PreprocessClass::GetFormsHash(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize, int form_size)
{
	int distance, index1, index2, index3, index4;
	multimap<int, ProteinPosition>::iterator it2;
	bool skip = false;
	string formSeq;
	formSeq.resize(form_size);
	unsigned int i, j, k, b, limit;
	for (j = 0; j < formTypes.size(); j++)
	{
		//Create the forms with their position in the text according to the proteins positions
		it2 = proteinsPositions.begin();
		while (it2 != proteinsPositions.end())
		{
			if (((*it2).second.endIndex - (*it2).second.startIndex) < formTypes[j].formType.size())
			{
				//cout << protein << endl;
				it2++;
				continue;
			}
			limit = ((*it2).second.endIndex - formTypes[j].formType.size());
			for (i = (*it2).second.startIndex; i <= limit; i++)
			{
				//Get the form
				distance = 0;
				b = 0;
				for (k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash(db[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
					{
						formSeq[b] = db[i + distance];
						b++;
					}
				}
				if (!skip)
				{
					//Check if the form already exists in the form list, if not then create a new form, else add its appearance position in the text
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					if (forms[index1][index2][index3][index4] == NULL)
						forms[index1][index2][index3][index4] = new Form(formTypes.size(), j, i);
					else
						forms[index1][index2][index3][index4]->addPosition(j, i);
				}
				skip = false;
			}
			it2++;
		}
	}
}

void PreprocessClass::GetFormsHash5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize, int form_size)
{
	int distance, index1, index2, index3, index4, index5;
	multimap<int, ProteinPosition>::iterator it2;
	bool skip = false;
	string formSeq;
	formSeq.resize(form_size);
	unsigned int i, j, k, b, limit;
	for (j = 0; j < formTypes.size(); j++)
	{
		//Create the forms with their position in the text according to the proteins positions
		it2 = proteinsPositions.begin();
		while (it2 != proteinsPositions.end())
		{
			if (((*it2).second.endIndex - (*it2).second.startIndex) < formTypes[j].formType.size())
			{
				//cout << protein << endl;
				it2++;
				continue;
			}
			limit = ((*it2).second.endIndex - formTypes[j].formType.size());
			for (i = (*it2).second.startIndex; i <= limit; i++)
			{
				//Get the form
				distance = 0;
				b = 0;
				for (k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash(db[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
					{
						formSeq[b] = db[i + distance];
						b++;
					}
				}
				if (!skip)
				{
					//Check if the form already exists in the form list, if not then create a new form, else add its appearance position in the text
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					index5 = AminoToIndexHash(formSeq[4]);
					if (forms[index1][index2][index3][index4][index5] == NULL)
						forms[index1][index2][index3][index4][index5] = new Form(formTypes.size(), j, i);
					else
						forms[index1][index2][index3][index4][index5]->addPosition(j, i);
				}
				skip = false;
			}
			it2++;
		}
	}
}

void PreprocessClass::CreateFormTypes(vector<FormType> &formTypes, int seqSize, int form_size, int mismatches)
{
	stringstream filename, ss;
	int i, j;
	int lastOne;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	ifstream myfile;
	myfile.open(filename.str());
	string line, form, formType;
	vector<string> formTypesStrings;
	while (getline(myfile, line) && line != "")
		formTypesStrings.push_back(line);
	myfile.close();

	for (i = 0; i < formTypesStrings.size(); i++)
	{
		FormType formType;
		formType.formType = formTypesStrings[i];
		lastOne = 0;
		for (j = 0; j < formTypesStrings[i].size(); j++)
		{
			if ((formTypesStrings[i])[j] == '1')
			{
				formType.distances.push_back(j - lastOne);
				lastOne = j;
			}
		}
		formTypes.push_back(formType);
	}
}

void PreprocessClass::PreProcess(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize)
{
	CreateFormTypes(formTypes, seqSize, form_size, mismatches);
	GetFormsHash(forms, formTypes, db, proteinsPositions, seqSize, form_size);
	/*ReadFormsASCII(forms, proteins, formTypes);
	cout << "Number of forms from ASCII hash: " << NumberOfForms << endl;*/
}

void PreprocessClass::PreProcess5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize)
{
	CreateFormTypes(formTypes, seqSize, form_size, mismatches);
	GetFormsHash5(forms, formTypes, db, proteinsPositions, seqSize, form_size);
	/*ReadFormsASCII5(forms, proteins, formTypes);
	cout << "Number of forms from ASCII hash: " << NumberOfForms << endl;*/
}

void PreprocessClass::ReadFormsUnorderedMap(unordered_map<string, Form*> &forms, string &db, multimap<int, ProteinPosition> proteinsPositions, vector<FormType> &formTypes)
{
	vector<vector<unsigned int>> *positions;
	//Loop through all form types
	for (int j = 0; j < formTypes.size(); j++)
	{
		string formSeq;
		int distance;
		unordered_map<string, Form*>::iterator it;
		unsigned int i;
		int k;
		//Create the forms with their position in the text according to the proteins positions
		bool skip = false;
		multimap<int, ProteinPosition>::iterator it2 = proteinsPositions.begin();
		while (it2 != proteinsPositions.end())
		{
			if (((*it2).second.endIndex - (*it2).second.startIndex) < formTypes[j].formType.size())
			{
				//cout << protein << endl;
				it2++;
				continue;
			}
			for (i = (*it2).second.startIndex; i <= ((*it2).second.endIndex - formTypes[j].formType.size()); i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash(db[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += db[i + distance];
				}
				if (!skip)
				{
					it = forms.find(formSeq);
					positions = (*it).second->getPositions();
				}
				skip = false;
			}
			it2++;
		}
	}
}

void PreprocessClass::ReadFormsASCII(vector<vector<vector<vector<Form*>>>> &forms, string &db, multimap<int, ProteinPosition> &proteinsPositions, vector<FormType> &formTypes)
{
	vector<vector<unsigned int>> *positions;
	for (int j = 0; j < formTypes.size(); j++)
	{
		int distance, index1, index2, index3, index4;
		bool skip = false;
		string formSeq;
		unsigned int i;
		int k;
		//Create the forms with their position in the text according to the proteins positions
		multimap<int, ProteinPosition>::iterator it2 = proteinsPositions.begin();
		while (it2 != proteinsPositions.end())
		{
			if (((*it2).second.endIndex - (*it2).second.startIndex) < formTypes[j].formType.size())
			{
				//cout << protein << endl;
				it2++;
				continue;
			}
			for (i = (*it2).second.startIndex; i <= ((*it2).second.endIndex - formTypes[j].formType.size()); i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash(db[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += db[i + distance];
				}
				if (!skip)
				{
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					positions = forms[index1][index2][index3][index4]->getPositions();
				}
				skip = false;
			}
			it2++;
		}
	}
}

void PreprocessClass::ReadFormsASCII5(vector<vector<vector<vector<vector<Form*>>>>> &forms, string &db, multimap<int, ProteinPosition> &proteinsPositions, vector<FormType> &formTypes)
{
	vector<vector<unsigned int>> *positions;
	for (int j = 0; j < formTypes.size(); j++)
	{
		int distance, index1, index2, index3, index4, index5;
		bool skip = false;
		string formSeq;
		unsigned int i;
		int k;
		//Create the forms with their position in the text according to the proteins positions
		multimap<int, ProteinPosition>::iterator it2 = proteinsPositions.begin();
		while (it2 != proteinsPositions.end())
		{
			if (((*it2).second.endIndex - (*it2).second.startIndex) < formTypes[j].formType.size())
			{
				//cout << protein << endl;
				it2++;
				continue;
			}
			for (i = (*it2).second.startIndex; i <= ((*it2).second.endIndex - formTypes[j].formType.size()); i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash(db[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += db[i + distance];
				}
				if (!skip)
				{
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					index5 = AminoToIndexHash(formSeq[4]);
					positions = forms[index1][index2][index3][index4][index5]->getPositions();
				}
				skip = false;
			}
			it2++;
		}
	}
}
