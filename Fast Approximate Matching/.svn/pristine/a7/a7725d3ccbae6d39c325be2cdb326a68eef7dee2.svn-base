/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include "Form.h"
#include <algorithm>
#include <sstream>
#include <fstream>
#include <ppl.h>
#include <mutex>
#include <iostream>
#include <set>

using namespace std;
using namespace concurrency;

const int NumberOfAminoAcids = 20;

struct FormType
{
	string formType;
	vector<int> distances;
};

void GetFormsSequence(unordered_map<string, Form*> &forms, vector<FormType> &formTypes, string &text);

void PreProcess(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, vector<string> &proteins, int seqSize);

void PreProcess5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, vector<string> &proteins, int seqSize);

bool CheckIfPreProcessed(int form_size, int mismatches, int seqSize);

void GetForms(unordered_map<string, Form*> &forms, vector<FormType> &formTypes, vector<string> &proteins, int seqSize);

int GetFormsHash(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, vector<string> &proteins, int seqSize);

int GetFormsHash5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, vector<string> &proteins, int seqSize);

void InitializeHash();

inline int AminoToIndexHash(char amino);

void ReadFormsUnorderedMap(unordered_map<string, Form*> &forms, vector<string> &proteins, vector<FormType> &formTypes);

void ReadFormsASCII(vector<vector<vector<vector<Form*>>>> &forms, vector<string> &proteins, vector<FormType> &formTypes);

void ReadFormsASCII5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<string> &proteins, vector<FormType> &formTypes);