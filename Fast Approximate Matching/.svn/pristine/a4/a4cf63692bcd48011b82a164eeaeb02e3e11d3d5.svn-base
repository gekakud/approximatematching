/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "FindSequence.h"

void FindSequenceClass::InitializeHashFind()
{
	int i, j;
	for (i = 0; i < 255; i++)
		AATabQuickFind[i] = 20;

	for (i = 0; i < 255; i++)
		for (j = 0; j < 20; j++)
			if (AAcode[j] == i)
				AATabQuickFind[i] = j;
}

void FindSequenceClass::changeAminoIndex(char changeFrom, char changeTo)
{
	AATabQuickFind[changeFrom] = AATabQuickFind[changeTo];
}

//Find sequence in a text
void FindSequenceClass::FindSequence(string &db, unordered_set<unsigned int> &positions, vector<unsigned int> &proteinsIndexes, vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, string &sequence, int form_size, int mismatches, int seqSize)
{
	unordered_map<string, Form*> sequenceForms;
	/*string newSequence;
	newSequence.resize(seqSize);
	replaceAminoAcids(sequence, newSequence);*/
	//Geting forms from the sequence
	GetFormsSequence(sequenceForms, forms, formTypes, sequence, form_size);
	unordered_map<string, Form*>::iterator it = sequenceForms.begin();
	unordered_map<string, Form*>::iterator endIt = sequenceForms.end();
	Form* form;
	int index1, index2, index3, index4;
	int k;
	unsigned int i, j, b, positionIndex, proteinNumber;
	unsigned char nonMatchCounter, matchCounter;
	unsigned char matches = seqSize - mismatches;
	unsigned int positionsInSequenceSize, positionsInSequenceFormTypeSize, positionsInTextFormTypeSize;

	while (it != endIt)
	{
		index1 = AminoToIndexHashFind(((*it).first)[0]);
		index2 = AminoToIndexHashFind(((*it).first)[1]);
		index3 = AminoToIndexHashFind(((*it).first)[2]);
		index4 = AminoToIndexHashFind(((*it).first)[3]);
		form = forms[index1][index2][index3][index4];
		//Positions that the form appear in the db
		vector<vector<unsigned int>>* positionsInText = form->getPositions();
		//Positions that the form appear in the sequence
		vector<vector<unsigned int>>* positionsInSequence = (*it).second->getPositions();
		positionsInSequenceSize = positionsInSequence->size();
		//Go through all form types in the Form object
		for (unsigned int formTypeIndex = 0; formTypeIndex < positionsInSequenceSize; formTypeIndex++)
		{
			/*If the form types vector is not empty in both the db and the sequence
			it means that there is a possible match in the positions that appear
			in those vectors.*/
			if (!(*positionsInSequence)[formTypeIndex].empty() && !(*positionsInText)[formTypeIndex].empty())
			{
				//Going through the positions of the form type
				positionsInSequenceFormTypeSize = (*positionsInSequence)[formTypeIndex].size();
				positionsInTextFormTypeSize = (*positionsInText)[formTypeIndex].size();
				//For each positions of the form type in the sequence
				for (i = 0; i < positionsInSequenceFormTypeSize; i++)
				{
					//For each positions of the form type in the db
					for (j = 0; j < positionsInTextFormTypeSize; j++)
					{
						/*Check the possibility that a positions in sequence is larger than the positions
						in the DB, skip this positions if it's true.*/
						if (((*positionsInText)[formTypeIndex])[j] >= ((*positionsInSequence)[formTypeIndex])[i])
						{
							/*Saving the position in the db that there could be a match starting from that position.*/
							positionIndex = ((*positionsInText)[formTypeIndex])[j] - ((*positionsInSequence)[formTypeIndex])[i];
							nonMatchCounter = 0;
							matchCounter = 0;
							/*Go through the DB from the positionIndex and check if the substring there
							has no more than the requested mismatches.*/
							proteinNumber = proteinsIndexes[positionIndex];
							for (k = 0, b = positionIndex; k < seqSize && b < db.size() && proteinsIndexes[b] == proteinNumber; k++, b++)
							{
								if (db[b] != sequence[k])
									nonMatchCounter++;
								else
									matchCounter++;

								if (nonMatchCounter > mismatches)
									break;
							}
							if (matchCounter >= matches)
								positions.insert(positionIndex);
						}
					}
				}
			}
		}
		it->second->~Form();
		it++;
	}
}

void FindSequenceClass::FindSequence5(string &db, unordered_set<unsigned int> &positions, vector<unsigned int> &proteinsIndexes, vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, string &sequence, int form_size, int mismatches, int seqSize)
{
	unordered_map<string, Form*> sequenceForms;
	/*string newSequence;
	newSequence.resize(seqSize);
	replaceAminoAcids(sequence, newSequence);*/
	//Geting forms from the sequence
	GetFormsSequence5(sequenceForms, forms, formTypes, sequence, form_size);
	unordered_map<string, Form*>::iterator it = sequenceForms.begin();
	unordered_map<string, Form*>::iterator endIt = sequenceForms.end();
	Form* form;
	int index1, index2, index3, index4, index5;
	int k;
	unsigned int i, j, b, positionIndex, proteinNumber;
	unsigned char nonMatchCounter, matchCounter;
	unsigned char matches = seqSize - mismatches;
	unsigned int positionsInSequenceSize, positionsInSequenceFormTypeSize, positionsInTextFormTypeSize;

	while (it != endIt)
	{
		index1 = AminoToIndexHashFind(((*it).first)[0]);
		index2 = AminoToIndexHashFind(((*it).first)[1]);
		index3 = AminoToIndexHashFind(((*it).first)[2]);
		index4 = AminoToIndexHashFind(((*it).first)[3]);
		index5 = AminoToIndexHashFind(((*it).first)[4]);
		form = forms[index1][index2][index3][index4][index5];
		//Positions that the form appear in the db
		vector<vector<unsigned int>>* positionsInText = form->getPositions();
		//Positions that the form appear in the sequence
		vector<vector<unsigned int>>* positionsInSequence = (*it).second->getPositions();
		positionsInSequenceSize = positionsInSequence->size();
		//Go through all form types in the Form object
		for (unsigned int formTypeIndex = 0; formTypeIndex < positionsInSequenceSize; formTypeIndex++)
		{
			/*If the form types vector is not empty in both the db and the sequence
			it means that there is a possible match in the positions that appear
			in those vectors.*/
			if (!(*positionsInSequence)[formTypeIndex].empty() && !(*positionsInText)[formTypeIndex].empty())
			{
				//Going through the positions of the form type
				positionsInSequenceFormTypeSize = (*positionsInSequence)[formTypeIndex].size();
				positionsInTextFormTypeSize = (*positionsInText)[formTypeIndex].size();
				//For each positions of the form type in the sequence
				for (i = 0; i < positionsInSequenceFormTypeSize; i++)
				{
					//For each positions of the form type in the db
					for (j = 0; j < positionsInTextFormTypeSize; j++)
					{
						/*Check the possibility that a positions in sequence is larger than the positions
						in the DB, skip this positions if it's true.*/
						if (((*positionsInText)[formTypeIndex])[j] >= ((*positionsInSequence)[formTypeIndex])[i])
						{
							/*Saving the position in the db that there could be a match starting from that position.*/
							positionIndex = ((*positionsInText)[formTypeIndex])[j] - ((*positionsInSequence)[formTypeIndex])[i];
							nonMatchCounter = 0;
							matchCounter = 0;
							/*Go through the DB from the positionIndex and check if the substring there
							has no more than the requested mismatches.*/
							proteinNumber = proteinsIndexes[positionIndex];
							for (k = 0, b = positionIndex; k < seqSize && b < db.size() && proteinsIndexes[b] == proteinNumber; k++, b++)
							{
								if (db[b] != sequence[k])
									nonMatchCounter++;
								else
									matchCounter++;

								if (nonMatchCounter > mismatches)
									break;
							}
							if (matchCounter >= matches)
								positions.insert(positionIndex);
						}
					}
				}
			}
		}
		it->second->~Form();
		it++;
	}
}
