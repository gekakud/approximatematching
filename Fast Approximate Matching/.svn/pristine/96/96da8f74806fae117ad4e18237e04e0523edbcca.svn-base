/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "Main.h"

string db;
multimap<int, ProteinPosition> proteinsPositions;
vector<string> proteinsNames;
int mismatches, form_size;
vector<string> sequences;

PreprocessClass preprocessClass;
FindSequenceClass findSeqClass;

//The preprocess results will be saved to the following vectors and will be stored in memory
vector<vector<vector<vector<Form*>>>> forms(NumberOfAminoAcids, vector<vector<vector<Form*>>>
(NumberOfAminoAcids, vector<vector<Form*>>(NumberOfAminoAcids, vector<Form*>(NumberOfAminoAcids, NULL))));

vector<vector<vector<vector<vector<Form*>>>>> forms5(NumberOfAminoAcids, vector<vector<vector<vector<Form*>>>>
(NumberOfAminoAcids, vector<vector<vector<Form*>>>(NumberOfAminoAcids, vector<vector<Form*>>
(NumberOfAminoAcids, vector<Form*>(NumberOfAminoAcids, NULL)))));

vector<FormType> formTypes;

struct SequenceResults
{
	string sequence;
	unordered_set<unsigned int> positions;
};

vector<unsigned int> frequencyCalc()
{
	unsigned int i;
	vector<unsigned int> frequencyTable(NumberOfAminoAcids, 0);
	for (i = 0; i < db.size(); i++)
		frequencyTable[preprocessClass.AminoToIndexHash(db[i])]++;
	return frequencyTable;
}

vector<char> groupLeastFrequent(vector<unsigned int> &frequencyTable)
{
	multimap<unsigned int, char> sortedFrequencies;
	vector<char> leastFrequentGroup;
	unsigned int i;
	for (i = 0; i < frequencyTable.size(); i++)
	{
		pair<unsigned int, char> tempPair = make_pair(frequencyTable[i], AAcode[i]);
		sortedFrequencies.insert(tempPair);
	}

	multimap<unsigned int, char>::iterator iter;
	iter = sortedFrequencies.begin();
	unsigned int maxFrequency = sortedFrequencies.rbegin()->first;
	unsigned int addFrequencies = 0;
	while (iter != sortedFrequencies.end())
	{
		addFrequencies += iter->first;
		if (addFrequencies <= maxFrequency)
			leastFrequentGroup.push_back(iter->second);
		else
			break;
		iter++;
	}
	return leastFrequentGroup;
}

void replaceAminoAcids(vector<char> leastFrequentGroup)
{
	/*Need to replace ASCII hash for the least popular
	amino acids to only one hash*/
	for (unsigned int i = 1; i < leastFrequentGroup.size(); i++)
	{
		preprocessClass.changeAminoIndex(leastFrequentGroup[i], leastFrequentGroup[0]);
		findSeqClass.changeAminoIndex(leastFrequentGroup[i], leastFrequentGroup[0]);
	}
}

/*Naive algorithm for the k-mismatch problem, used to check the run time
differences between naive algorithm and our algorithm*/
void NaiveAlgorithm(string &sequence, int mismatches, unordered_set<unsigned int> &positions)
{
	unsigned char nonMatchCounter, matchCounter;
	unsigned int i, j, k;
	multimap<int, ProteinPosition>::iterator iter = proteinsPositions.begin();
	unsigned char matches = SEQ_SIZE - mismatches;
	while (iter != proteinsPositions.end())
	{
		for (i = iter->second.startIndex; i <= iter->second.endIndex; i++)
		{
			nonMatchCounter = 0;
			matchCounter = 0;
			for (j = 0, k = i; k <= iter->second.endIndex && j < sequence.size(); j++, k++)
			{
				if (db[k] != sequence[j])
					nonMatchCounter++;
				else
					matchCounter++;

				if (nonMatchCounter > mismatches)
					break;
			}
			if (matchCounter >= matches)
				positions.insert(i);
		}
		iter++;
	}
}

//Generate a random DB with equal probability for each amino acid
void GenerateRandomDBEqual(string path, string fileName)
{
	unsigned int i, j, proteinPosition;
	unsigned int times = 50;
	unsigned int numberOfProteins = 30000;
	string str;
	ifstream myfile(path + fileName);
	if (!myfile.good())
	{
		for (i = 0; i < NumberOfAminoAcids; i++)
			for (j = 0; j < times; j++)
				str.push_back(AAcode[i]);
		for (i = 0; i < numberOfProteins; i++)
		{
			proteinPosition = db.size();
			ProteinPosition newPositions;
			newPositions.startIndex = proteinPosition;
			//Shuffle a string randomally
			random_shuffle(str.begin(), str.end());
			db.append(str);
			newPositions.endIndex = db.size() - 1;
			pair<int, ProteinPosition> tempPair = make_pair(i, newPositions);
			proteinsPositions.insert(tempPair);
			proteinsNames.push_back(str.substr(0, 100));
		}
		ofstream myfile2;
		myfile2.open(path + fileName);
		myfile2 << db;
		myfile2.close();
	}
	else
	{
		myfile >> db;
		for (i = 0; i < numberOfProteins; i++)
		{
			ProteinPosition newPositions;
			newPositions.startIndex = i * times * NumberOfAminoAcids;
			newPositions.endIndex = i * times * NumberOfAminoAcids + times * NumberOfAminoAcids - 1;
			pair<int, ProteinPosition> tempPair = make_pair(i, newPositions);
			proteinsPositions.insert(tempPair);
			proteinsNames.push_back(db.substr(newPositions.startIndex, 100));
		}
	}
	myfile.close();
}

//Print the search results of a single sequence
void PrintPositionsResults(unordered_set<unsigned int> &positions)
{
	unordered_set<unsigned int>::iterator iter, iterEnd;
	multimap<int, ProteinPosition>::iterator it3, it3End;
	int proteinIndex, proteinNumber;
	iter = positions.begin();
	iterEnd = positions.end();
	it3End = proteinsPositions.end();
	while (iter != iterEnd)
	{
		it3 = proteinsPositions.begin();
		while (it3 != it3End)
		{
			if (((*it3).second.startIndex <= *iter) && ((*it3).second.endIndex >= *iter))
			{
				proteinNumber = (*it3).first;
				proteinIndex = *iter - (*it3).second.startIndex;
				break;
			}
			it3++;
		}
		cout << "Protein number: " << proteinNumber << endl;
		cout << "Protein name: " << proteinsNames[proteinNumber] << endl;
		cout << "Index in protein: " << proteinIndex << endl;
		if ((*iter + SEQ_SIZE) <= (*it3).second.endIndex)
			cout << "Sequence in protein: " << db.substr(*iter, SEQ_SIZE) << endl << endl;
		else
			cout << "Sequence in protein: " << db.substr(*iter, (*it3).second.endIndex - *iter) << endl << endl;
		iter++;
	}
}

//Print the search results of a single sequence for GUI
void PrintPositionsResultsGUI(unordered_set<unsigned int> &positions)
{
	unordered_set<unsigned int>::iterator iter, iterEnd;
	multimap<int, ProteinPosition>::iterator it3, it3End;
	int proteinIndex, proteinNumber;
	iter = positions.begin();
	iterEnd = positions.end();
	it3End = proteinsPositions.end();
	while (iter != iterEnd)
	{
		it3 = proteinsPositions.begin();
		while (it3 != it3End)
		{
			if (((*it3).second.startIndex <= *iter) && ((*it3).second.endIndex >= *iter))
			{
				proteinNumber = (*it3).first;
				proteinIndex = *iter - (*it3).second.startIndex;
				break;
			}
			it3++;
		}
		cout << proteinNumber << endl;
		cout << proteinsNames[proteinNumber] << endl;
		cout << proteinIndex << endl;
		if ((*iter + SEQ_SIZE) <= (*it3).second.endIndex)
			cout << db.substr(*iter, SEQ_SIZE) << endl << endl;
		else
			cout << db.substr(*iter, (*it3).second.endIndex - *iter) << endl << endl;
		iter++;
	}
}

void PrintSequencesResults(vector<SequenceResults> &sequencesResults)
{
	unordered_set<unsigned int>::iterator iter2, iter2End;
	multimap<int, ProteinPosition>::iterator it3, it3End;
	unsigned int i;
	int proteinIndex, proteinNumber;

	//For each sequence, print its search results
	for (i = 0; i < sequencesResults.size(); i++)
	{
		cout << "Sequence: " << sequencesResults[i].sequence << endl;
		cout << "Matched positions in DB:" << endl;
		iter2 = sequencesResults[i].positions.begin();
		iter2End = sequencesResults[i].positions.end();
		while (iter2 != iter2End)
		{
			it3 = proteinsPositions.begin();
			it3End = proteinsPositions.end();
			while (it3 != it3End)
			{
				if (((*it3).second.startIndex <= *iter2) && ((*it3).second.endIndex >= *iter2))
				{
					proteinNumber = (*it3).first;
					proteinIndex = *iter2 - (*it3).second.startIndex;
					break;
				}
				it3++;
			}
			cout << "Protein number: " << proteinNumber << endl;
			cout << "Protein name: " << proteinsNames[proteinNumber] << endl;
			cout << "Index in protein: " << proteinIndex << endl;
			cout << "Sequence in protein: " << db.substr(*iter2, SEQ_SIZE) << endl << endl;
			iter2++;
		}
		cout << endl << endl;
	}
	cout << endl << endl;
}

/*A function that is called by a thread to find a sequence in the database*/
void startFind(string parameters)
{
	double algorithmRunTime, naiveRunTime;
	//clock_t begin, end;
	unordered_set<unsigned int> positions;
	stringstream ss;
	string sequence;
	int mismatches, form_size;
	int threadIndex;
	ss << parameters;
	ss >> threadIndex >> sequence >> mismatches >> form_size;

	//begin = clock();

	/*A function that finds the sequence in the database, the function will
	write to the positions vector the positions were there is a match*/
	if (form_size == 4)
		findSeqClass.FindSequence(db, positions, forms, formTypes, sequence, form_size, mismatches, SEQ_SIZE);
	else if (form_size == 5)
		findSeqClass.FindSequence5(db, positions, forms5, formTypes, sequence, form_size, mismatches, SEQ_SIZE);

	//end = clock();
	//algorithmRunTime = getTime(begin, end);

	//cout << endl << "Our algorithm results:" << endl;

	//PrintPositionsResults(positions); //For command line
	PrintPositionsResultsGUI(positions); //For GUI
	cout << "end" << endl;

	positions.clear();

	//NaiveAlgorithm(sequence, mismatches, positions);
	//PrintPositionsResults(positions);
}

void startFindAlgorithm(string parameters)
{
	unordered_set<unsigned int> positions;
	vector<SequenceResults> sequencesResults;
	stringstream ss;
	unsigned int startSequence, endSequence, i;
	ss << parameters;
	ss >> startSequence >> endSequence;

	/*A function that finds the sequence in the database, the function will
	write to the positions vector the positions were there is a match*/
	for (i = startSequence; i < endSequence; i++)
	{
		if (form_size == 4)
			findSeqClass.FindSequence(db, positions, forms, formTypes, sequences[i], form_size, mismatches, SEQ_SIZE);
		else if (form_size == 5)
			findSeqClass.FindSequence5(db, positions, forms5, formTypes, sequences[i], form_size, mismatches, SEQ_SIZE);
		if (positions.size() > 0)
		{
			SequenceResults newResults;
			newResults.sequence = sequences[i];
			newResults.positions = positions;
			sequencesResults.push_back(newResults);
		}
		positions.clear();
	}
	PrintSequencesResults(sequencesResults);
}

void startFindNaive(string parameters)
{
	unordered_set<unsigned int> positions;
	vector<SequenceResults> sequencesResults;
	stringstream ss;
	unsigned int startSequence, endSequence, i;
	ss << parameters;
	ss >> startSequence >> endSequence;

	/*A function that finds the sequence in the database, the function will
	write to the positions vector the positions were there is a match*/
	for (i = startSequence; i < endSequence; i++)
	{
		NaiveAlgorithm(sequences[i], mismatches, positions);
		if (positions.size() > 0)
		{
			SequenceResults newResults;
			newResults.sequence = sequences[i];
			newResults.positions = positions;
			sequencesResults.push_back(newResults);
		}
		positions.clear();
	}
	PrintSequencesResults(sequencesResults);
}

void ReadDB(string path, string fileName)
{
	int i;
	ifstream myfile;
	string line;
	myfile.open(path + fileName);
	if (!myfile.good())
	{
		cout << "DB file does not exist" << endl;
		cin.get();
		exit(1);
	}
	cout << "DB read started" << endl;
	int proteinIndex = 0;
	int proteinPosition = 0;
	int proteinSize = 0;
	vector <string> name;
	bool reachedSize = false;
	string temp;

	//Read the database and add each protein and its name to vectors
	while (myfile >> line)
	{
		if (line == "sz")
			reachedSize = true;
		if (reachedSize)
		{
			myfile >> proteinSize;
			myfile >> temp;
			i = 0;
			while (temp.size() < proteinSize || temp.substr(temp.size() - 3, temp.size()) == "fas")
			{
				myfile >> temp;
				if (temp.size() < proteinSize || i == 0)
				{
					name.push_back(temp);
					if (i == 0 && temp.size() >= proteinSize)
						myfile >> temp;
				}
				i++;
			}
			proteinsNames.push_back(name[0]);
			for (i = 0; i < name.size(); i++)
			{
				proteinsNames[proteinsNames.size() - 1].append(" ");
				proteinsNames[proteinsNames.size() - 1].append(name[i]);
			}
			name.clear();
			proteinPosition = db.size();
			db.append(temp);
			reachedSize = false;



			ProteinPosition newPositions;
			newPositions.startIndex = proteinPosition;
			newPositions.endIndex = db.size() - 1;
			pair<int, ProteinPosition> tempPair2 = make_pair(proteinIndex, newPositions);
			proteinsPositions.insert(tempPair2);
			proteinIndex++;
		}
	}
	myfile.close();
	cout << "DB read finished" << endl;
	ofstream myfileWrite;
	myfileWrite.open("New" + fileName);
	myfileWrite << db;
	myfileWrite.close();
}

void InitializePreprocess(int mismatches, int form_size)
{
	//Currently only form sizes of 4 and 5 are implemented
	if (form_size != 4 && form_size != 5)
	{
		cout << "Form size is invalid" << endl;
		cin.get();
		exit(1);
	}

	/*Initialize ASCII hash, both functions are doing exactly the same thing
	but they are divided between 2 classes and are used by inline functions.*/
	preprocessClass.InitializeHash();
	findSeqClass.InitializeHashFind();

	//vector<unsigned int> frequencyTable = frequencyCalc();
	//vector<char> leastFrequentGroup = groupLeastFrequent(frequencyTable);
	//replaceAminoAcids(leastFrequentGroup);

	//Check if there is already a file that contain the preprocessed form types results
	cout << "Preprocess started" << endl;
	//Check if a form types set file exist
	bool preprocessed = preprocessClass.CheckIfPreProcessed(form_size, mismatches, SEQ_SIZE);
	if (!preprocessed)
	{
		cout << "Form types file is missing" << endl;
		cin.get();
		exit(1);
	}
	else
	{
		if (form_size == 4)
			preprocessClass.PreProcess(forms, formTypes, form_size, mismatches, db, proteinsPositions, SEQ_SIZE);
		if (form_size == 5)
			preprocessClass.PreProcess5(forms5, formTypes, form_size, mismatches, db, proteinsPositions, SEQ_SIZE);
	}

	//Delete ASCII forms in case of using form size 5 (too much RAM)
	//PROCESS_MEMORY_COUNTERS pmc;
	//GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	//cout << pmc.WorkingSetSize / (1024.0 * 1024.0 * 1024.0) << " GB in use after ASCII preprocess" << endl;
	/*unordered_map<string, Form*> forms2;
	GetForms(forms2, formTypes, proteins, SEQ_SIZE);
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	cout << pmc.WorkingSetSize / (1024.0 * 1024.0 * 1024.0) << " GB in use after unordered_map preprocess" << endl;
	begin = clock();
	ReadFormsUnorderedMap(forms2, proteins, formTypes);
	end = clock();

	cout << endl;
	cout << "Number of forms from unordered_map: " << forms2.size() << endl;
	cout << "Access time using unordered_map hash: " << getTime(begin, end) << " sec" << endl;*/

	cout << "Preprocess finished" << endl;
}

void GetInput(int mismatches, int form_size)
{
	string parameters = "";

	while (parameters != "q")
	{
		/*Get an input from the command line, if the input is 'q' then end the program, ignore empty input*/
		getline(cin, parameters);
		if (parameters == "q")
			break;
		int mismatchesGet, formSizeGet, preprocessIndex;
		string temp;
		stringstream ss2, ss3;
		ss2 << parameters;
		//Get the input mismatches and form size
		ss2 >> temp;
		if (!(ss2 >> mismatchesGet))
		{
			mismatchesGet = mismatches;
			ss2 << mismatchesGet;
		}
		if (!(ss2 >> formSizeGet))
		{
			formSizeGet = form_size;
			ss2 << formSizeGet;
		}
		ss3 << threads.size() << " " << temp << " " << mismatchesGet << " " << formSizeGet;
		parameters = ss3.str();

		if (formSizeGet != form_size || mismatchesGet > mismatches)
		{
			cout << "Wrong arguments" << endl;
			continue;
		}

		if (temp.size() != SEQ_SIZE)
		{
			cout << "The sequence size is not " << SEQ_SIZE << endl;
			continue;
		}

		//Create a thread to find the input sequence in the database
		//int threadIndex = threads.size();
		//ThreadStruct ts;
		//ts.th = new thread(&startFind, parameters);
		//ts.alive = true;

		thread th = thread(&startFind, parameters);
		th.join();

		//pair<int, ThreadStruct> tempPair = make_pair(threadIndex, ts);
		//threads.insert(tempPair);
		parameters = "";

		//Check if there are dead threads and delete them from the threads vector
		/*for (unsigned int threadIndex = 0; threadIndex < threads.size(); threadIndex++)
		{
			if (threads[threadIndex].alive == false)
			{
				threads[threadIndex].th->join();
				threads.erase(threads.begin() + threadIndex);
			}
		}*/
	}

	//Wait for threads execution and delete them from the threads vector
	//for (unsigned int threadIndex = 0; threadIndex < threads.size(); threadIndex++)
	//	threads[threadIndex].th->join();
}

void GetSequences(string sequencesPath, string sequencesFileName, int mismatches, int form_size)
{
	double algorithmRunTime, naiveRunTime;
	ifstream myfile;
	string line;
	unsigned int i, threadIndex;
	myfile.open(sequencesPath + sequencesFileName);
	if (!myfile.good())
	{
		cout << "Sequences file does not exist" << endl;
		cin.get();
		exit(1);
	}

	while (getline(myfile, line) && line != "")
		sequences.push_back(line);
	myfile.close();

	unsigned int startSequence, endSequence;
	unsigned int N = std::thread::hardware_concurrency(); //Number of CPU cores
	unsigned int numOfSeq = sequences.size() / N; //Number of sequences per thread


												  //Our algorithm
	auto begin = std::chrono::steady_clock::now();
	//No threading
	startSequence = 0;
	endSequence = sequences.size();
	stringstream ss2;
	ss2 << startSequence << " " << endSequence;
	thread th = thread(&startFindAlgorithm, ss2.str());
	th.join();
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> diff = end - begin;
	std::cout << "Our algorithm run time (no threading): " << diff.count() << " sec" << endl;

	//Threading
	auto begin2 = std::chrono::steady_clock::now();

	for (i = 0; i < N; i++)
	{
		startSequence = i * numOfSeq;
		endSequence = startSequence + numOfSeq - 1;
		stringstream ss;
		ss << startSequence << " " << endSequence;
		ThreadStruct ts;
		ts.th = new thread(&startFindAlgorithm, ss.str());
		ts.index = threads.size();
		ts.alive = true;
		threads.push_back(ts);
	}
	for (threadIndex = 0; threadIndex < threads.size(); threadIndex++)
	{
		threads[threadIndex].th->join();
		delete threads[threadIndex].th;
	}
	auto end2 = std::chrono::steady_clock::now();
	std::chrono::duration<double> diff2 = end2 - begin2;
	std::cout << "Our algorithm run time (with threading): " << diff2.count() << " sec" << endl;

	threads.clear();



	//Naive algorithm
	/*auto begin3 = std::chrono::steady_clock::now();
	//No threading
	startSequence = 0;
	endSequence = sequences.size();
	stringstream ss3;
	ss3 << startSequence << " " << endSequence;
	th = thread(&startFindNaive, ss3.str());
	th.join();
	auto end3 = std::chrono::steady_clock::now();
	std::chrono::duration<double> diff3 = end3 - begin3;
	std::cout << "Naive algorithm run time (no threading): " << diff3.count() << " sec" << endl;

	//Threading
	auto begin4 = std::chrono::steady_clock::now();
	for (i = 0; i < N; i++)
	{
		startSequence = i * numOfSeq;
		endSequence = startSequence + numOfSeq - 1;
		stringstream ss;
		ss << startSequence << " " << endSequence;
		ThreadStruct ts;
		ts.th = new thread(&startFindNaive, ss.str());
		ts.index = threads.size();
		ts.alive = true;
		threads.push_back(ts);
	}
	for (threadIndex = 0; threadIndex < threads.size(); threadIndex++)
	{
		threads[threadIndex].th->join();
		delete threads[threadIndex].th;
	}
	auto end4 = std::chrono::steady_clock::now();
	std::chrono::duration<double> diff4 = end4 - begin4;
	std::cout << "Naive algorithm run time (with threading): " << diff4.count() << " sec" << endl;

	threads.clear();

	double runTime1 = diff3 / diff;
	double runTime2 = diff4 / diff2;
	cout << "Our algorithm is " << runTime1 << " times faster than naive algorithm (no threading)" << endl << endl << endl;
	cout << "Our algorithm is " << runTime2 << " times faster than naive algorithm (with threading)" << endl << endl << endl;*/
}

/*Usage:
1) If you want to search for individual sequences in DB:
On the command line, run the exe file like this:
file.exe [db file] [mismatches] [form_size]
For example: file.exe C:\\db.txt 8 4

2) If you want to search a list of sequences in DB:
On the command line, run the exe file like this:
file.exe [db file] [mismatches] [form_size]
For example: file.exe -l C:\\db.txt C:\\SequencesList.txt 8 4
*/
int main(int argc, char *argv[])
{
	string DBFileName, DBpath, sequencesFileName, sequencesPath;
	int flag = 0;
	//Protein database
	/*If the program is executed like this for example:
	file.exe -l db.txt 8 4
	The flag '-l' make it so the program will get a sequences file
	and search those sequences on the DB.*/
	if (argc >= 2 && (argv[1])[0] == '-' && (argv[1])[1] == 'l')
	{
		flag = 2;
		DBFileName = argv[flag];
		sequencesFileName = argv[1 + flag];
	}
	/*If there is no 'l' flag than the program will do a preprocess
	and when it is done, it will wait for an input of a sequence from
	the user.*/
	else if (argc >= 2)
		DBFileName = argv[1 + flag];
	else
		DBFileName = "E:\\ProteinDB4.txt";

	//Divide the path from the filename
	for (int i = DBFileName.size(); i >= 0; i--)
	{
		if (DBFileName[i] == '/')
		{
			DBpath = DBFileName.substr(0, i) + '/';
			DBFileName = DBFileName.substr(i + 1, DBFileName.size());
		}
	}
	if (flag == 2 && (argv[1])[0] == '-' && (argv[1])[1] == 'l')
	{
		for (int i = sequencesFileName.size(); i >= 0; i--)
		{
			if (sequencesFileName[i] == '/')
			{
				sequencesPath = sequencesFileName.substr(0, i) + '/';
				sequencesFileName = sequencesFileName.substr(i + 1, sequencesFileName.size());
			}
		}
	}

	//if (argc >= 2)
	//    GenerateRandomDBEqual(DBpath, DBFileName);
	ReadDB(DBpath, DBFileName);

	//Get the number of mismatches and form size from the command line or set them to 8 and 4 as default
	mismatches = (argc >= 3) ? atoi(argv[2 + flag]) : 8;
	form_size = (argc >= 4) ? atoi(argv[3 + flag]) : 4;

	InitializePreprocess(mismatches, form_size);

	/*PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	cout << pmc.WorkingSetSize / (1024.0 * 1024.0 * 1024.0) << " GB in use after ASCII preprocess" << endl;*/

	//GetSequences("E:\\", "SequencesList.txt", mismatches, form_size);
	if (flag == 2)
		//Search for a list of sequences
		GetSequences(sequencesPath, sequencesFileName, mismatches, form_size);
	else
		//Wait for user to input sequence one by one
		GetInput(mismatches, form_size);

	cout << "End of program" << endl;

	if (argc <= 1)
		cin.get();
	return 0;
}
