/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "PreProcess.h"

void CalculateFormTypes(vector<string> &formTypes, int form_size, int mismatches, int seqSize)
{
	/*Optimization: Get all the combinations, for each combination get all the form types, for each form type there will be a counter of
	how many times it appears in the combinations. Then, we will go from the most frequent to the least frequent form type, we will search the
	form type in each combination and delete the rest of the form types from that combination, in the end there should be a list of
	combinations with only one form type, we will take the remaining form types and put it in the formTypes vector.*/
	formTypes.clear();
	map<string, vector<string>> combinations, combinationsToDelete;
	map<string, int> allFormTypes;
	vector<string> emptyVector;
	int i, j, k;
	string ones = "";

	//Create a starting combination of 0's and 1's
	for (i = 0; i < seqSize; i++)
		ones += '1';
	fill(ones.begin(), ones.begin() + mismatches, '0');

	//Get all the combinations using the algorithm next_permutation
	do {
		pair<string, vector<string>> tempPair = make_pair(ones, emptyVector);
		combinations.insert(tempPair);
	} while (next_permutation(ones.begin(), ones.end()));

	//For each combination make a vector of form types
	map<string, vector<string>>::iterator it = combinations.begin();
	int onesCounter = 0;
	string temp;
	while (it != combinations.end())
	{
		for (j = 0; j < seqSize; j++)
		{
			if ((*it).first[j] == '1')
			{
				for (k = j; k < seqSize; k++)
				{
					if ((*it).first[k] == '1')
						onesCounter++;
					if (onesCounter == form_size)
						break;
				}
				temp = (*it).first.substr(j, k - j + 1);
				if (onesCounter == form_size && (find((*it).second.begin(), (*it).second.end(), temp) == (*it).second.end()))
					(*it).second.push_back(temp);
				onesCounter = 0;
			}
		}
		it++;
	}

	//Get all unique form types and a count of how many combinations they appear on
	it = combinations.begin();
	map<string, int>::iterator it2;
	while (it != combinations.end())
	{
		for (i = 0; i < (*it).second.size(); i++)
		{
			it2 = allFormTypes.find((*it).second[i]);
			if (it2 == allFormTypes.end())
			{
				pair<string, int> tempPair = make_pair((*it).second[i], 1);
				allFormTypes.insert(tempPair);
			}
			else
				(*it2).second++;
		}
		it++;
	}

	//Reverse allFormTypes in order to sort the form types according to their appearance
	multimap<int, string> allFormTypesReverse;
	it2 = allFormTypes.begin();
	while (it2 != allFormTypes.end())
	{
		pair<int, string> tempPair = make_pair((*it2).second, (*it2).first);
		allFormTypesReverse.insert(tempPair);
		it2++;
	}

	/*Go through the remaining form types and for every combination, ignore the least frequent form types and add
	the most frequent form type to the optimized form type vector*/
	combinationsToDelete = combinations;
	string combToDelete;
	map<int, string>::reverse_iterator it3 = allFormTypesReverse.rbegin();
	while (it3 != allFormTypesReverse.rend())
	{
		it = combinationsToDelete.begin();
		while (it != combinationsToDelete.end())
		{
			if (find((*it).second.begin(), (*it).second.end(), (*it3).second) != (*it).second.end())
			{
				if (find(formTypes.begin(), formTypes.end(), (*it3).second) == (formTypes.end()))
					formTypes.push_back((*it3).second);
				combToDelete = (*it).first;
				it++;
				combinationsToDelete.erase(combToDelete);
			}
			else
				it++;
		}
		it3++;
	}

	//Zakharia form types for form size 5
	/*formTypes.clear();
	formTypes.push_back("11111");
	formTypes.push_back("111101");
	formTypes.push_back("111011");
	formTypes.push_back("1111001");
	formTypes.push_back("1110101");
	formTypes.push_back("1101101");
	formTypes.push_back("110111");
	formTypes.push_back("1110011");
	formTypes.push_back("1101011");
	formTypes.push_back("11110001");
	formTypes.push_back("11101001");
	formTypes.push_back("11011001");
	formTypes.push_back("11100101");
	formTypes.push_back("11010101");
	formTypes.push_back("111100001");
	formTypes.push_back("111010001");
	formTypes.push_back("110110001");
	formTypes.push_back("10101101");
	formTypes.push_back("101110001");
	formTypes.push_back("111001001");
	formTypes.push_back("110101001");
	formTypes.push_back("101101001");
	formTypes.push_back("11100011");
	formTypes.push_back("11010011");
	formTypes.push_back("10110011");
	formTypes.push_back("110011001");
	formTypes.push_back("101011001");
	combinationsToDelete = combinations;
	for (j = 0; j < formTypes.size(); j++)
	{
		it = combinationsToDelete.begin();
		while (it != combinationsToDelete.end())
		{
			if (find((*it).second.begin(), (*it).second.end(), formTypes[j]) != (*it).second.end())
			{
				combToDelete = (*it).first;
				it++;
				combinationsToDelete.erase(combToDelete);
			}
			else
				it++;
		}
	}

	cout << "Combinations left for original form types list: " << combinationsToDelete.size() << endl;
	it = combinationsToDelete.begin();
	while (it != combinationsToDelete.end())
	{
		cout << (*it).first << endl;
		it++;
	}*/

	/*Second optimization: Go through each form type in the formTypes vector and check if there are 
	redundant form types there.*/
	vector<string> formTypesTest;
	bool optimized = false;
	int indexToIgnore = 0;
	while (!optimized)
	{
		for (i = 0; i < formTypes.size(); i++)
		{
			if (indexToIgnore != i)
				formTypesTest.push_back(formTypes[i]);
		}

		combinationsToDelete = combinations;
		for (j = 0; j < formTypesTest.size(); j++)
		{
			it = combinationsToDelete.begin();
			while (it != combinationsToDelete.end())
			{
				if (find((*it).second.begin(), (*it).second.end(), formTypesTest[j]) != (*it).second.end())
				{
					combToDelete = (*it).first;
					it++;
					combinationsToDelete.erase(combToDelete);
				}
				else
					it++;
			}
		}
		if (combinationsToDelete.size() == 0)
		{
			formTypes = formTypesTest;
			if (indexToIgnore == formTypes.size())
				optimized = true;
		}
		else
			indexToIgnore++;
		formTypesTest.clear();
		if (indexToIgnore >= formTypes.size())
			optimized = true;
	}
}

void GetForms(unordered_map<string, Form*> &forms, vector<string> &formTypes, string &text, multimap<int, map<int, int>> &proteinsPositions)
{
	mutex mutex;
	//Loop through all form types
	parallel_for(size_t(0), formTypes.size(), [&](size_t j)
	{
		string substring, formSeq;
		unordered_map<string, Form*>::iterator it;
		//Create the forms with their position in the text according to the proteins positions
		multimap<int, map<int, int>>::iterator it2 = proteinsPositions.begin();
		while (it2 != proteinsPositions.end())
		{
			string protein = text.substr((*it2).second.begin()->first, (*it2).second.begin()->second - (*it2).second.begin()->first);
			for (int i = 0; i < protein.size() - formTypes[j].size() + 1; i++)
			{
				//Get a substring in the position i with the size of the current form type
				substring = protein.substr(i, formTypes[j].size());
				stringstream formstream;
				//Get the form
				for (int k = 0; k < substring.size(); k++)
					if ((formTypes[j])[k] == '1')
						formstream << substring[k];
				formSeq = formstream.str();
				//Check if the form already exists in the form list, if not then create a new form, else add its appearence position in the text
				it = forms.find(formSeq);
				if (it == forms.end())
				{
					Form *newForm = new Form(formSeq, formTypes[j], i + (*it2).second.begin()->first);
					pair<string, Form*> tempPair = make_pair(formSeq, newForm);
					mutex.lock();
					forms.insert(tempPair);
					mutex.unlock();
				}
				else
				{
					mutex.lock();
					(*it).second->addPosition(formTypes[j], i + (*it2).second.begin()->first);
					mutex.unlock();
				}
			}
			it2++;
		}
	});
}

void GetFormsSequence(unordered_map<string, Form*> &forms, vector<string> &formTypes, string &text)
{
	string substring, formSeq;
	unordered_map<string, Form*>::iterator it;
	//Loop through all form types
	for (int j = 0; j < formTypes.size(); j++)
	{
		//Create the forms with their position in the text
		for (int i = 0; i < text.size() - formTypes[j].size() + 1; i++)
		{
			//Get a substring in the position i with the size of the current form type
			substring = text.substr(i, formTypes[j].size());
			stringstream formstream;
			//Get the form
			for (int k = 0; k < substring.size(); k++)
				if ((formTypes[j])[k] == '1')
					formstream << substring[k];
			formSeq = formstream.str();
			//Check if the form already exists in the form list, if not then create a new form, else add its appearence position in the text
			it = forms.find(formSeq);
			if (it == forms.end())
			{
				Form *newForm = new Form(formSeq, formTypes[j], i);
				pair<string, Form*> tempPair = make_pair(formSeq, newForm);
				forms.insert(tempPair);
			}
			else
				(*it).second->addPosition(formTypes[j], i);
		}
	}
}

void PreProcess(unordered_map<string, Form*> &forms, vector<string> &formTypes, int form_size, int mismatches, string &text, multimap<int, map<int, int>> &proteinsPositions, int seqSize)
{
	/*Most frequent form types that cover all 20 letters combinations with 4 form size and 8 mismatches
	calculated by running CalculateFormTypes function.*/
	/*formTypes.push_back("1111");
	formTypes.push_back("11101");
	formTypes.push_back("10111");
	formTypes.push_back("11011");
	formTypes.push_back("111001");
	formTypes.push_back("110101");*/
	//Get form types
	CalculateFormTypes(formTypes, form_size, mismatches, seqSize);
	GetForms(forms, formTypes, text, proteinsPositions);

	//Save Preprocess form types results to a file
	stringstream filename;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	ofstream myfile;
	myfile.open(filename.str());
	for (int i = 0; i < formTypes.size(); i++)
		myfile << formTypes[i] << endl;

	myfile.close();
}

bool CheckIfPreProcessed(int form_size, int mismatches, int seqSize)
{
	stringstream filename;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	ifstream myfile(filename.str());
	return myfile.good();
}

void getResults(unordered_map<string, Form*> &forms, vector<string> &formTypes, int form_size, int mismatches, string &text, multimap<int, map<int, int>> &proteinsPositions, int seqSize)
{
	stringstream filename, ss;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	ifstream myfile;
	myfile.open(filename.str());
	string line, form, formType;
	while (getline(myfile, line) && line != "")
		formTypes.push_back(line);
	GetForms(forms, formTypes, text, proteinsPositions);
	myfile.close();
}