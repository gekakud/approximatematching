/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "FindSequence.h"

//Find sequence in a text
void FindSequence(string &text, vector<int> &positions, unordered_map<string, Form*> &forms, vector<string> &formTypes, string &sequence, int form_size, int mismatches, int seqSize)
{
	unordered_map<string, Form*> sequenceForms;
	//clock_t begin, end;

	GetFormsSequence(sequenceForms, formTypes, sequence);

	unordered_map<string, Form*>::iterator it = sequenceForms.begin();
	unordered_map<string, Form*>::iterator it2;
	multimap<string, vector<int>>::iterator it3, iter3;
	vector<int>::iterator it4, it5;
	vector<int> possiblePositionsInText, possiblePositionsInSequence, realPositionsInText;
	int i, j;
	//Saving positions that were already checked for a match in an array for a complexity of O(1)
	unordered_map<int, int> positionsToCheck;
	unordered_map<int, int>::iterator posIter;

	//begin = clock();
	while (it != sequenceForms.end())
	{
		it2 = forms.find((*it).second->getFormSequence());
		if (it2 != forms.end())
		{
			multimap<string, vector<int>>* positionsInText = (*it2).second->getPositions();
			multimap<string, vector<int>>* positionsInSequence = (*it).second->getPositions();

			//Delete formTypes in text that don't exist in sequence
			it3 = (*positionsInText).begin();
			while (it3 != (*positionsInText).end())
			{
				if ((*positionsInSequence).find((*it3).first) == (*positionsInSequence).end())
				{
					string posToDelete = (*it3).first;
					it3++;
					(*positionsInText).erase(posToDelete);
				}
				else
					it3++;
			}

			//Delete formTypes in sequence that don't exist in text
			it3 = (*positionsInSequence).begin();
			while (it3 != (*positionsInSequence).end())
			{
				if ((*positionsInText).find((*it3).first) == (*positionsInText).end())
				{
					string posToDelete = (*it3).first;
					it3++;
					(*positionsInSequence).erase(posToDelete);
				}
				else
					it3++;
			}

			//For every position in text, duplicate position in sequence
			it3 = (*positionsInSequence).begin();
			while (it3 != (*positionsInSequence).end())
			{
				iter3 = (*positionsInText).find((*it3).first);
				if (iter3 != (*positionsInText).end())
				{
					for (i = 0; i < (*iter3).second.size() - 1; i++)
					{
						(*it).second->addPosition((*it3).first, (*it3).second[i]);
					}
				}
				it3++;
			}

			//Add possible positions for a match in text and sequence
			iter3 = (*positionsInSequence).begin();
			it3 = (*positionsInText).begin();
			while (iter3 != (*positionsInSequence).end())
			{
				for (i = 0; i < (*iter3).second.size(); i++)
				{
					posIter = positionsToCheck.find((*it3).second[i] - (*iter3).second[i]);
					if (posIter == positionsToCheck.end())
					{
						pair<int, int> tempPair = make_pair((*it3).second[i] - (*iter3).second[i], 1);
						positionsToCheck.insert(tempPair);
					}
					else if ((*posIter).second >= ceil(((seqSize - mismatches) / double(form_size)) - 1))
					{
						possiblePositionsInText.push_back((*it3).second[i]);
						possiblePositionsInSequence.push_back((*iter3).second[i]);
						(*posIter).second = -1;
					}
					else if ((*posIter).second != -1)
						(*posIter).second++;
				}
				it3++;
				iter3++;
			}
		}
		it++;
	}
	//end = clock();
	//cout << "Time to find possible matches: " << getTime(begin, end) << endl;

	//begin = clock();

	it4 = possiblePositionsInSequence.begin();
	it5 = possiblePositionsInText.begin();
	int matchCounter;

	//For every possible position, check if there is a k-mismatch in that position
	//begin = clock();
	while (it4 != possiblePositionsInSequence.end())
	{
		int positionIndex = (*it5) - (*it4);
		matchCounter = 0;
		for (int i = 0; i < sequence.size(); i++)
		{
			if (text[positionIndex + i] == sequence[i])
			{
				matchCounter++;
				if (matchCounter >= seqSize - mismatches)
					break;
			}
		}
		if (matchCounter >= seqSize - mismatches)
			positions.push_back(positionIndex);
		it4++;
		it5++;
	}
	//end = clock();
	//cout << "Time to find matches using the possible matches vector: " << getTime(begin, end) << endl;
}