/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "FindSequence.h"

//Find sequence in a text
void FindSequence(string &db, vector<unsigned int> &positions, vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, string &sequence, int form_size, int mismatches, int seqSize)
{
	unordered_map<string, Form*> sequenceForms;
	clock_t begin, end;

	GetFormsSequence(sequenceForms, formTypes, sequence);

	unordered_map<string, Form*>::iterator it = sequenceForms.begin();
	string formName;
	int index1, index2, index3, index4;
	vector<unsigned int>::iterator it4, it5;
	vector<unsigned int> possiblePositionsInText, possiblePositionsInSequence, realPositionsInText;
	int i, j;
	//Saving positions that were already checked for a match in an array for a complexity of O(1)

	//Hash table, the first key is the protein index and the second key is the position in the protein
	unordered_map<unsigned int, int> positionsToCheck;
	unordered_map<unsigned int, int>::iterator posIter;

	//begin = clock();
	while (it != sequenceForms.end())
	{
		formName = (*it).first;
		index1 = AminoToIndexHash(formName[0]);
		index2 = AminoToIndexHash(formName[1]);
		index3 = AminoToIndexHash(formName[2]);
		index4 = AminoToIndexHash(formName[3]);
		if (forms[index1][index2][index3][index4] != NULL)
		{
			vector<vector<unsigned int>>* positionsInText = forms[index1][index2][index3][index4]->getPositions();
			vector<vector<unsigned int>>* positionsInSequence = (*it).second->getPositions();
			for (int formTypeIndex = 0; formTypeIndex < positionsInSequence->size(); formTypeIndex++)
			{
				if (!(*positionsInSequence)[formTypeIndex].empty() && !(*positionsInText)[formTypeIndex].empty())
				{
					for (i = 0; i < (*positionsInText)[formTypeIndex].size(); i++)
					{
						for (j = 0; j < (*positionsInSequence)[formTypeIndex].size(); j++)
						{
							posIter = positionsToCheck.find(((*positionsInText)[formTypeIndex])[i] - ((*positionsInSequence)[formTypeIndex])[j]);
							if (posIter == positionsToCheck.end())
							{
								pair<int, int> tempPair = make_pair(((*positionsInText)[formTypeIndex])[i] - ((*positionsInSequence)[formTypeIndex])[j], 1);
								positionsToCheck.insert(tempPair);
							}
							else if ((*posIter).second >= ceil(((seqSize - mismatches) / double(form_size)) - 1) && (*posIter).second != -1)
							{
								possiblePositionsInText.push_back(((*positionsInText)[formTypeIndex])[i]);
								possiblePositionsInSequence.push_back(((*positionsInSequence)[formTypeIndex])[j]);
								(*posIter).second = -1;
							}
							else if ((*posIter).second != -1)
								(*posIter).second++;
						}
					}
				}
			}
		}
		it++;
	}
	//end = clock();
	//cout << "Time to find possible matches: " << getTime(begin, end) << endl;

	//begin = clock();
	it4 = possiblePositionsInSequence.begin();
	it5 = possiblePositionsInText.begin();
	int matchCounter;

	//For every possible position, check if there is a k-mismatch in that position
	while (it4 != possiblePositionsInSequence.end())
	{
		int positionIndex = (*it5) - (*it4);
		matchCounter = 0;
		for (int i = 0; i < sequence.size(); i++)
		{
			if (db[positionIndex + i] == sequence[i])
			{
				matchCounter++;
				if (matchCounter >= seqSize - mismatches)
					break;
			}
		}
		if (matchCounter >= seqSize - mismatches)
			positions.push_back(positionIndex);
		it4++;
		it5++;
	}
	//end = clock();
	//cout << "Time to find matches using the possible matches vector: " << getTime(begin, end) << endl;
}