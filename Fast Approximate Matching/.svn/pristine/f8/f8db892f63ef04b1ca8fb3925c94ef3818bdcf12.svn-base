/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "Main.h"

string db;
multimap<int, ProteinPosition> proteinsPositions;
vector<string> proteinsNames;

//The preprocess results will be saved to the following vectors and will be stored in memory
vector<vector<vector<vector<Form*>>>> forms(NumberOfAminoAcids, vector<vector<vector<Form*>>>
(NumberOfAminoAcids, vector<vector<Form*>>(NumberOfAminoAcids, vector<Form*>(NumberOfAminoAcids, NULL))));

vector<vector<vector<vector<vector<Form*>>>>> forms5(NumberOfAminoAcids, vector<vector<vector<vector<Form*>>>>
(NumberOfAminoAcids, vector<vector<vector<Form*>>>(NumberOfAminoAcids, vector<vector<Form*>>
(NumberOfAminoAcids, vector<Form*>(NumberOfAminoAcids, NULL)))));

vector<FormType> formTypes;

/*Naive algorithm for the k-mismatch problem, used to check the run time
differences between naive algorithm and our algorithm*/
void NaiveAlgorithm(string &sequence, int mismatches, vector<unsigned int> &positions)
{
	int matchCounter;
	int i, j, k;
	for (i = 0; i < db.size(); i++)
	{
		matchCounter = 0;
		for (j = 0, k = i; k < db.size() && j < sequence.size(); j++, k++)
		{
			if (db[k] == sequence[j])
				matchCounter++;
		}
		if (matchCounter >= SEQ_SIZE - mismatches)
			positions.push_back(i);
	}
	cout << endl;
}

/*A function that is called by a thread to find a sequence in the database*/
void startFind(string parameters)
{
	clock_t begin, end;
	double algorithmRunTime, naiveRunTime;
	vector<unsigned int> positions;
	stringstream ss;
	string sequence;
	int mismatches, form_size;
	ss << parameters;
	ss >> sequence >> mismatches >> form_size;
	if (sequence.size() != SEQ_SIZE)
		cout << "The sequence size is not " << SEQ_SIZE << endl;
	else
	{
		begin = clock();

		/*A function that finds the sequence in the database, the function will
		write to the positions vector the positions were there is a match*/
		FindSequence(db, positions, forms, formTypes, sequence, form_size, mismatches, SEQ_SIZE);

		end = clock();
		algorithmRunTime = getTime(begin, end);
	}

	for (int i = 0; i < positions.size(); i++)
	{
		multimap<int, ProteinPosition>::iterator it3 = proteinsPositions.begin();
		int proteinIndex, proteinNumber;
		while (it3 != proteinsPositions.end())
		{
			if (((*it3).second.startIndex <= positions[i]) && ((*it3).second.endIndex >= positions[i]))
			{
				proteinNumber = (*it3).first;
				proteinIndex = positions[i] - (*it3).second.startIndex;
				break;
			}
			it3++;
		}
		cout << proteinNumber << endl << proteinsNames[proteinNumber] << endl;
		cout << proteinIndex << endl;
	}
	cout << "end" << endl;

	positions.clear();
	begin = clock();
	NaiveAlgorithm(sequence, mismatches, positions);
	end = clock();

	cout << "Our algorithm run time: " << algorithmRunTime << " sec" << endl << endl;
	cout << "Naive algorithm results:" << endl;
	for (int i = 0; i < positions.size(); i++)
	{
		multimap<int, ProteinPosition>::iterator it3 = proteinsPositions.begin();
		int proteinIndex, proteinNumber;
		while (it3 != proteinsPositions.end())
		{
			if (((*it3).second.startIndex <= positions[i]) && ((*it3).second.endIndex >= positions[i]))
			{
				proteinNumber = (*it3).first;
				proteinIndex = positions[i] - (*it3).second.startIndex;
				break;
			}
			it3++;
		}
		cout << proteinNumber << endl << proteinsNames[proteinNumber] << endl;
		cout << proteinIndex << endl;
	}

	naiveRunTime = getTime(begin, end);

	cout << "Naive algorithm run time: " << naiveRunTime << " sec" << endl << endl;
	double runTime = algorithmRunTime == 0 ? naiveRunTime * 1000 : naiveRunTime / algorithmRunTime;
	cout << "Our algorithm is " << runTime << " times faster than naive algorithm" << endl << endl << endl;
}

/*Usage: On the command line, run the exe file like this:
file.exe [db file] [mismatches] [form_size]
For example: file.exe "C:\\db.txt" 8 4
*/
int main(int argc, char *argv[])
{
	clock_t begin, end;
	int form_size;
	int mismatches;
	stringstream ss;
	ifstream myfile;
	int i;
	string fileName, path;
	//Protein database
	if (argc >= 2)
		fileName = argv[1];
	else
		fileName = "E:\\ProteinDB3.txt";
	for (i = fileName.size(); i >= 0; i--)
	{
		if (fileName[i] == '\\')
		{
			path = fileName.substr(0, i) + '\\';
			fileName = fileName.substr(i + 1, fileName.size());
		}
	}
	string line;
	myfile.open(path + fileName);
	if (!myfile.good())
	{
		cout << "File does not exist" << endl;
		cin.get();
		return 1;
	}
	cout << "DB read started" << endl;

	int proteinIndex = 0;
	int proteinPosition = 0;
	int proteinSize = 0;
	vector <string> name;
	bool reachedSize = false;
	string temp;

	//Read the database and add each protein and its name to vectors
	while (myfile >> line)
	{
		if (line == "sz")
			reachedSize = true;
		if (reachedSize)
		{
			myfile >> proteinSize;
			myfile >> temp;
			i = 0;
			while (temp.size() < proteinSize || temp.substr(temp.size() - 3, temp.size()) == "fas")
			{
				myfile >> temp;
				if (temp.size() < proteinSize || i == 0)
				{
					name.push_back(temp);
					if (i == 0 && temp.size() >= proteinSize)
						myfile >> temp;
				}
				i++;
			}
			proteinsNames.push_back(name[0]);
			for (i = 0; i < name.size(); i++)
			{
				proteinsNames[proteinsNames.size() - 1].append(" ");
				proteinsNames[proteinsNames.size() - 1].append(name[i]);
			}
			name.clear();
			proteinPosition = db.size();
			db.append(temp);
			reachedSize = false;



			ProteinPosition newPositions;
			newPositions.startIndex = proteinPosition;
			newPositions.endIndex = db.size();
			pair<int, ProteinPosition> tempPair2 = make_pair(proteinIndex, newPositions);
			proteinsPositions.insert(tempPair2);
			proteinIndex++;
		}
	}
	myfile.close();
	cout << "DB read finished" << endl;
	ofstream myfileWrite;
	myfileWrite.open("New" + fileName);
	myfileWrite << db;
	myfileWrite.close();

	//Get the number of mismatches and form size from the command line or set them to 8 and 4 as default
	mismatches = (argc >= 3) ? atoi(argv[2]) : 8;
	form_size = (argc >= 4) ? atoi(argv[3]) : 4;

	//Currently only form sizes of 4 and 5 are implemented
	if (form_size != 4 && form_size != 5)
	{
		cout << "Form size is invalid" << endl;
		cin.get();
		return 1;
	}

	InitializeHash();

	//Check if there is already a file that contain the preprocessed form types results
	cout << "Preprocess started" << endl;
	bool preprocessed = CheckIfPreProcessed(form_size, mismatches, SEQ_SIZE);
	if (!preprocessed)
	{
		cout << "Form types file is missing" << endl;
		cin.get();
		return 1;
	}
	else
	{
		if (form_size == 4)
			PreProcess(forms, formTypes, form_size, mismatches, db, proteinsPositions, SEQ_SIZE);
		if (form_size == 5)
			PreProcess5(forms5, formTypes, form_size, mismatches, db, proteinsPositions, SEQ_SIZE);
	}

	//Delete ASCII forms in case of using form size 5 (too much RAM)
	PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	cout << pmc.WorkingSetSize / (1024.0 * 1024.0 * 1024.0) << " GB in use after ASCII preprocess" << endl;
	/*unordered_map<string, Form*> forms2;
	GetForms(forms2, formTypes, proteins, SEQ_SIZE);
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	cout << pmc.WorkingSetSize / (1024.0 * 1024.0 * 1024.0) << " GB in use after unordered_map preprocess" << endl;
	begin = clock();
	ReadFormsUnorderedMap(forms2, proteins, formTypes);
	end = clock();

	cout << endl;
	cout << "Number of forms from unordered_map: " << forms2.size() << endl;
	cout << "Access time using unordered_map hash: " << getTime(begin, end) << " sec" << endl;*/

	cout << "Preprocess finished" << endl;

	string parameters = "";

	while (parameters != "q")
	{
		/*Get an input from the command line, if the input is 'q' then end the program, ignore empty input*/
		getline(cin, parameters);
		if (parameters.size() >= SEQ_SIZE)
		{
			int mismatchesGet, formSizeGet, preprocessIndex;
			string temp;
			stringstream ss2, ss3;
			ss2 << parameters;
			//Get the input mismatches and form size
			ss2 >> temp;
			if (!(ss2 >> mismatchesGet))
			{
				mismatchesGet = mismatches;
				ss2 << mismatchesGet;
			}
			if (!(ss2 >> formSizeGet))
			{
				formSizeGet = form_size;
				ss2 << formSizeGet;
			}
			ss3 << temp << " " << mismatchesGet << " " << formSizeGet;
			parameters = ss3.str();

			bool preprocessedUser = CheckIfPreProcessed(formSizeGet, mismatchesGet, SEQ_SIZE);
			if (!preprocessedUser)
			{
				//cout << "Wrong arguments" << endl;
				//continue;
			}

			//Check if there are dead threads and delete them from the threads vector
			for (int i = 0; i < threads.size(); i++)
			{
				if (threads[i].joinable())
				{
					threads[i].join();
					threads.erase(threads.begin() + i);
				}
			}
			//Create a thread to find the input sequence in the database
			threads.push_back(thread(startFind, parameters));
			parameters = "";
		}
	}

	for (int i = 0; i < threads.size(); i++)
	{
		threads[i].join();
		threads.erase(threads.begin() + i);
	}

	if (form_size == 4)
	{
		for (int i1 = 0; i1 < forms.size(); i1++)
		{
			for (int i2 = 0; i2 < forms[i1].size(); i2++)
			{
				for (int i3 = 0; i3 < forms[i1][i2].size(); i3++)
				{
					for (int i4 = 0; i4 < forms[i1][i2][i3].size(); i4++)
					{
						if (forms[i1][i2][i3][i4] != NULL)
							forms[i1][i2][i3][i4]->~Form();
					}
				}
			}
		}
	}
	else if (form_size == 5)
	{
		for (int i1 = 0; i1 < forms5.size(); i1++)
		{
			for (int i2 = 0; i2 < forms5[i1].size(); i2++)
			{
				for (int i3 = 0; i3 < forms5[i1][i2].size(); i3++)
				{
					for (int i4 = 0; i4 < forms5[i1][i2][i3].size(); i4++)
					{
						for (int i5 = 0; i5 < forms5[i1][i2][i3][i4].size(); i5++)
						{
							if (forms5[i1][i2][i3][i4][i5] != NULL)
								forms5[i1][i2][i3][i4][i5]->~Form();
						}
					}
				}
			}
		}
	}

	if (argc <= 1)
		cin.get();
	return 0;
}