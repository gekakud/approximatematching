/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#include "PreProcess.h"

const char AAcode[] = { 'A', 'R', 'N', 'D', 'C', 'E', 'Q', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' };

int AATabQuick[255];

void GetFormsSequence(unordered_map<string, Form*> &forms, vector<FormType> &formTypes, string &text)
{
	string formSeq;
	int distance;
	bool skip = false;
	unordered_map<string, Form*>::iterator it;
	//Loop through all form types
	for (int j = 0; j < formTypes.size(); j++)
	{
		//Create the forms with their position in the text
		for (int i = 0; i < text.size() - formTypes[j].formType.size() + 1; i++)
		{
			//Get the form
			formSeq = "";
			distance = 0;
			for (int k = 0; k < formTypes[j].distances.size(); k++)
			{
				distance += formTypes[j].distances[k];
				if (AminoToIndexHash(text[i + distance]) == 20)
				{
					skip = true;
					break;
				}
				else
					formSeq += text[i + distance];
			}
			if (!skip)
			{
				//Check if the form already exists in the form list, if not then create a new form, else add its appearence position in the text
				it = forms.find(formSeq);
				if (it == forms.end())
				{
					Form *newForm = new Form(formSeq, j, -1, i);
					pair<string, Form*> tempPair = make_pair(formSeq, newForm);
					forms.insert(tempPair);
				}
				else
					(*it).second->addPosition(j, -1, i);
			}
			skip = false;
		}
	}
}

bool CheckIfPreProcessed(int form_size, int mismatches, int seqSize)
{
	stringstream filename;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	ifstream myfile(filename.str());
	return myfile.good();
}

void InitializeHash()
{
	int i, j;
	for (i = 0; i<255; i++)
		AATabQuick[i] = 20;

	for (i = 0; i<255; i++)
		for (j = 0; j<20; j++)
			if (AAcode[j] == i)
				AATabQuick[i] = j;
}

inline int AminoToIndexHash(char amino)
{
	return AATabQuick[amino];
}

void GetForms(unordered_map<string, Form*> &forms, vector<FormType> &formTypes, vector<string> &proteins, int seqSize)
{
	mutex mutex;
	//Loop through all form types
	//for (int j = 0; j < formTypes.size(); j++)
	parallel_for(size_t(0), formTypes.size(), [&](size_t j)
	{
		string formSeq;
		int distance;
		unordered_map<string, Form*>::iterator it;
		//Create the forms with their position in the text according to the proteins positions
		bool skip = false;
		int proteinIndex;
		for (proteinIndex = 0; proteinIndex < proteins.size(); proteinIndex++)
		{
			if (proteins[proteinIndex].size() < formTypes[j].formType.size())
				continue;
			for (int i = 0; i < proteins[proteinIndex].size() - formTypes[j].formType.size() + 1; i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (int k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash((proteins[proteinIndex])[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += (proteins[proteinIndex])[i + distance];
				}
				if (!skip)
				{
					//Check if the form already exists in the form list, if not then create a new form, else add its appearence position in the text
					it = forms.find(formSeq);
					if (it == forms.end())
					{
						Form *newForm = new Form(formSeq, j, proteinIndex, i);
						pair<string, Form*> tempPair = make_pair(formSeq, newForm);
						mutex.lock();
						forms.insert(tempPair);
						mutex.unlock();
					}
					else
					{
						mutex.lock();
						(*it).second->addPosition(j, proteinIndex, i);
						mutex.unlock();
					}
				}
				skip = false;
			}
		}
	});
	//}
}

int GetFormsHash(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, vector<string> &proteins, int seqSize)
{
	mutex mutex;
	int numberOfForms = 0;
	//for (int j = 0; j < formTypes.size(); j++)
	parallel_for(size_t(0), formTypes.size(), [&](size_t j)
	{
		int distance, index1, index2, index3, index4;
		bool skip = false;
		string formSeq;
		//Create the forms with their position in the text according to the proteins positions
		int proteinIndex;
		for (proteinIndex = 0; proteinIndex < proteins.size(); proteinIndex++)
		{
			if (proteins[proteinIndex].size() < formTypes[j].formType.size())
				continue;
			for (int i = 0; i < proteins[proteinIndex].size() - formTypes[j].formType.size() + 1; i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (int k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash((proteins[proteinIndex])[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += (proteins[proteinIndex])[i + distance];
				}
				if (!skip)
				{
					//Check if the form already exists in the form list, if not then create a new form, else add its appearance position in the text
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					mutex.lock();
					if (forms[index1][index2][index3][index4] == NULL)
					{
						forms[index1][index2][index3][index4] = new Form(formSeq, j, proteinIndex, i);
						numberOfForms++;
					}
					else
						forms[index1][index2][index3][index4]->addPosition(j, proteinIndex, i);
					mutex.unlock();
				}
				skip = false;
			}
		}
	});
	//}
	return numberOfForms;
}

int GetFormsHash5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, vector<string> &proteins, int seqSize)
{
	mutex mutex;
	int numberOfForms = 0;
	//for (int j = 0; j < formTypes.size(); j++)
	parallel_for(size_t(0), formTypes.size(), [&](size_t j)
	{
		int distance, index1, index2, index3, index4, index5;
		bool skip = false;
		string formSeq;
		//Create the forms with their position in the text according to the proteins positions
		int proteinIndex;
		for (proteinIndex = 0; proteinIndex < proteins.size(); proteinIndex++)
		{
			if (proteins[proteinIndex].size() < formTypes[j].formType.size())
				continue;
			for (int i = 0; i < proteins[proteinIndex].size() - formTypes[j].formType.size() + 1; i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (int k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash((proteins[proteinIndex])[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += (proteins[proteinIndex])[i + distance];
				}
				if (!skip)
				{
					//Check if the form already exists in the form list, if not then create a new form, else add its appearance position in the text
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					index5 = AminoToIndexHash(formSeq[4]);
					mutex.lock();
					if (forms[index1][index2][index3][index4][index5] == NULL)
					{
						forms[index1][index2][index3][index4][index5] = new Form(formSeq, j, proteinIndex, i);
						numberOfForms++;
					}
					else
						forms[index1][index2][index3][index4][index5]->addPosition(j, proteinIndex, i);
					mutex.unlock();
				}
				skip = false;
			}
		}
	});
	//}
	return numberOfForms;
}

void CreateFormTypes(vector<FormType> &formTypes, int seqSize, int form_size, int mismatches)
{
	stringstream filename, ss;
	int i, j;
	int lastOne;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	ifstream myfile;
	myfile.open(filename.str());
	string line, form, formType;
	vector<string> formTypesStrings;
	while (getline(myfile, line) && line != "")
		formTypesStrings.push_back(line);
	myfile.close();

	for (i = 0; i < formTypesStrings.size(); i++)
	{
		FormType formType;
		formType.formType = formTypesStrings[i];
		lastOne = 0;
		for (j = 0; j < formTypesStrings[i].size(); j++)
		{
			if ((formTypesStrings[i])[j] == '1')
			{
				formType.distances.push_back(j - lastOne);
				lastOne = j;
			}
		}
		formTypes.push_back(formType);
	}
}

void PreProcess(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, vector<string> &proteins, int seqSize)
{
	clock_t begin, end;
	CreateFormTypes(formTypes, seqSize, form_size, mismatches);
	int NumberOfForms = GetFormsHash(forms, formTypes, proteins, seqSize);
	begin = clock();
	ReadFormsASCII(forms, proteins, formTypes);
	end = clock();
	cout << "Number of forms from ASCII hash: " << NumberOfForms << endl;
	cout << "Access time using ASCII hash: " << getTime(begin, end) << " sec" << endl;
}

void PreProcess5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, vector<string> &proteins, int seqSize)
{
	clock_t begin, end;
	CreateFormTypes(formTypes, seqSize, form_size, mismatches);
	int NumberOfForms = GetFormsHash5(forms, formTypes, proteins, seqSize);
	begin = clock();
	ReadFormsASCII5(forms, proteins, formTypes);
	end = clock();
	cout << "Number of forms from ASCII hash: " << NumberOfForms << endl;
	cout << "Access time using ASCII hash: " << getTime(begin, end) << " sec" << endl;
}

void ReadFormsUnorderedMap(unordered_map<string, Form*> &forms, vector<string> &proteins, vector<FormType> &formTypes)
{
	string formName;
	//Loop through all form types
	for (int j = 0; j < formTypes.size(); j++)
	{
		string formSeq;
		int distance;
		unordered_map<string, Form*>::iterator it;
		//Create the forms with their position in the text according to the proteins positions
		bool skip = false;
		int proteinIndex;
		for (proteinIndex = 0; proteinIndex < proteins.size(); proteinIndex++)
		{
			if (proteins[proteinIndex].size() < formTypes[j].formType.size())
				continue;
			for (int i = 0; i < proteins[proteinIndex].size() - formTypes[j].formType.size() + 1; i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (int k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash((proteins[proteinIndex])[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += (proteins[proteinIndex])[i + distance];
				}
				if (!skip)
				{
					it = forms.find(formSeq);
					formName = (*it).second->getFormSequence();
				}
				skip = false;
			}
		}
	}
}

void ReadFormsASCII(vector<vector<vector<vector<Form*>>>> &forms, vector<string> &proteins, vector<FormType> &formTypes)
{
	string formName;
	for (int j = 0; j < formTypes.size(); j++)
	{
		int distance, index1, index2, index3, index4;
		bool skip = false;
		string formSeq;
		//Create the forms with their position in the text according to the proteins positions
		int proteinIndex;
		for (proteinIndex = 0; proteinIndex < proteins.size(); proteinIndex++)
		{
			if (proteins[proteinIndex].size() < formTypes[j].formType.size())
				continue;
			for (int i = 0; i < proteins[proteinIndex].size() - formTypes[j].formType.size() + 1; i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (int k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash((proteins[proteinIndex])[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += (proteins[proteinIndex])[i + distance];
				}
				if (!skip)
				{
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					formName = forms[index1][index2][index3][index4]->getFormSequence();
				}
				skip = false;
			}
		}
	}
}

void ReadFormsASCII5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<string> &proteins, vector<FormType> &formTypes)
{
	string formName;
	for (int j = 0; j < formTypes.size(); j++)
	{
		int distance, index1, index2, index3, index4, index5;
		bool skip = false;
		string formSeq;
		//Create the forms with their position in the text according to the proteins positions
		int proteinIndex;
		for (proteinIndex = 0; proteinIndex < proteins.size(); proteinIndex++)
		{
			if (proteins[proteinIndex].size() < formTypes[j].formType.size())
				continue;
			for (int i = 0; i < proteins[proteinIndex].size() - formTypes[j].formType.size() + 1; i++)
			{
				//Get the form
				formSeq = "";
				distance = 0;
				for (int k = 0; k < formTypes[j].distances.size(); k++)
				{
					distance += formTypes[j].distances[k];
					if (AminoToIndexHash((proteins[proteinIndex])[i + distance]) == 20)
					{
						skip = true;
						break;
					}
					else
						formSeq += (proteins[proteinIndex])[i + distance];
				}
				if (!skip)
				{
					index1 = AminoToIndexHash(formSeq[0]);
					index2 = AminoToIndexHash(formSeq[1]);
					index3 = AminoToIndexHash(formSeq[2]);
					index4 = AminoToIndexHash(formSeq[3]);
					index5 = AminoToIndexHash(formSeq[4]);
					formName = forms[index1][index2][index3][index4][index5]->getFormSequence();
				}
				skip = false;
			}
		}
	}
}