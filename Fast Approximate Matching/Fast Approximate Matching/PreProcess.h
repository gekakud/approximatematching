/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#ifndef PREPROCESS_H_
#define PREPROCESS_H_

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include "Form.h"
#include <sstream>
#include <fstream>
#include <mutex>
#include <iostream>
#include <set>

using namespace std;

class PreprocessClass
{
private:
	int AATabQuick[255];
public:
	void PreProcess(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize);

	void PreProcess5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, int form_size, int mismatches, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize);

	bool CheckIfPreProcessed(int form_size, int mismatches, int seqSize);

	void GetForms(unordered_map<string, Form*> &forms, vector<FormType> &formTypes, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize);

	void GetFormsHash(vector<vector<vector<vector<Form*>>>> &forms, vector<FormType> &formTypes, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize, int form_size);

	void GetFormsHash5(vector<vector<vector<vector<vector<Form*>>>>> &forms, vector<FormType> &formTypes, string &db, multimap<int, ProteinPosition> &proteinsPositions, int seqSize, int form_size);

	void CreateFormTypes(vector<FormType> &formTypes, int seqSize, int form_size, int mismatches);

	void InitializeHash();

	inline int AminoToIndexHash(char amino)
	{
		return AATabQuick[amino];
	}

	void changeAminoIndex(char changeFrom, char changeTo);

	void ReadFormsUnorderedMap(unordered_map<string, Form*> &forms, string &db, multimap<int, ProteinPosition> proteinsPositions, vector<FormType> &formTypes);

	void ReadFormsASCII(vector<vector<vector<vector<Form*>>>> &forms, string &db, multimap<int, ProteinPosition> &proteinsPositions, vector<FormType> &formTypes);

	void ReadFormsASCII5(vector<vector<vector<vector<vector<Form*>>>>> &forms, string &db, multimap<int, ProteinPosition> &proteinsPositions, vector<FormType> &formTypes);
};

#endif /* PREPROCESS_H_ */
