/*Author: Ben Peretz
Supervisor: Dr. Zakharia Frenkel*/

#ifndef MAIN_H_
#define MAIN_H_

#include <iostream>
#include <string>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include <vector>
#include <thread>
#include <chrono>
#include "Form.h"
#include "Preprocess.h"
#include "FindSequence.h"
//#include "windows.h"
//#include "psapi.h"

using namespace std;

struct ThreadStruct
{
	thread* th;
	bool alive;
};

struct SequenceResults
{
	string sequence;
	unordered_set<unsigned int> positions;
};

const int SEQ_SIZE = 20;
static vector<thread*> threads;
static mutex mutexThreads;

vector<unsigned int> frequencyCalc();

vector<char> groupLeastFrequent(vector<unsigned int> &frequencyTable);

void replaceAminoAcids(vector<char> leastFrequentGroup);

void NaiveAlgorithm(string &sequence, int mismatches, unordered_set<unsigned int> &positions);

void GenerateRandomDBEqual(string path, string fileName);

void PrintPositionsResults(unordered_set<unsigned int> &positions);

void PrintPositionsResultsGUI(unordered_set<unsigned int> &positions);

void PrintSequencesResults(vector<SequenceResults> &sequencesResults);

void startFind(string parameters);

void startFindAlgorithm(string parameters);

void startFindNaive(string parameters);

void ReadDB(string path, string fileName);

void InitializePreprocess(int mismatches, int form_size);

void threadsHandler();

void GetInput(int mismatches, int form_size);

void GetSequences(string sequencesPath, string sequencesFileName, int mismatches, int form_size);

int main(int argc, char *argv[]);


#endif /* MAIN_H_ */
