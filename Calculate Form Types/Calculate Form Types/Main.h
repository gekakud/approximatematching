#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <thread>
#include <iostream>
#include <map>
#include <mutex>
#include <algorithm>
#include <ppl.h>

using namespace std;
using namespace concurrency;

void GetCombinations();

void CountFormsCombination();

void GetUniqueFormTypes(map<unsigned short, int> &allFormTypes);

void ReverseFormTypes(map<unsigned short, int> &allFormTypes, multimap<int, unsigned short> &allFormTypesReverse);

void calcFrequencies(multimap<int, unsigned short> &allFormTypesReverse, map<string, vector<unsigned short>> &combinationsToDelete, int index);

bool FindFrequentFormTypes(vector<unsigned short> &formTypes, multimap<int, unsigned short> &allFormTypesReverse, int start, int cont);

bool FindFrequentFormTypes2(vector<unsigned short> &formTypes, multimap<int, unsigned short> &allFormTypesReverse, int start, int cont);

bool CheckRedundantForms(vector<unsigned short> &formTypes);

void CalculateFormTypes(vector<unsigned short> &formTypes);

void CountFormsCombination2(int start);

void CalculateFormTypes2(vector<unsigned short> &formTypes);

int main(int argc, char *argv[]);