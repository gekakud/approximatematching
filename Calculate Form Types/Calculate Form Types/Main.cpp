#include "Main.h"

map<string, vector<unsigned short>> combinations, combinationsWithoutForms;
vector<string> formTypesIndexes;
int seqSize, form_size, mismatches;
mutex mutexForms;
int numberOfThreads = 15;

void GetCombinations()
{
	string ones = "";
	vector<unsigned short> emptyVector;
	//Create a starting combination of 0's and 1's
	for (int i = 0; i < seqSize; i++)
		ones += '1';
	fill(ones.begin(), ones.begin() + mismatches, '0');

	//Get all the combinations using the algorithm next_permutation
	do {
		pair<string, vector<unsigned short>> tempPair = make_pair(ones, emptyVector);
		combinations.insert(tempPair);
	} while (next_permutation(ones.begin(), ones.end()));
	combinationsWithoutForms = combinations;
	cout << "Number of combinations: " << combinations.size() << endl;
}

void CountFormsCombination()
{
	//For each combination make a vector of form types
	map<string, vector<unsigned short>>::iterator it = combinations.begin();
	int onesCounter;
	string temp;
	vector<string>::iterator posIt;
	unsigned short pos;
	int j, k;
	while (it != combinations.end())
	{
		for (j = 0; j < seqSize; j++)
		{
			if ((*it).first[j] == '1')
			{
				onesCounter = 0;
				for (k = j; k < seqSize; k++)
				{
					if ((*it).first[k] == '1')
						onesCounter++;
					if (onesCounter == form_size)
						break;
				}
				if (onesCounter != form_size)
					continue;
				temp = (*it).first.substr(j, k - j + 1);
				posIt = find(formTypesIndexes.begin(), formTypesIndexes.end(), temp);
				if (posIt == formTypesIndexes.end())
				{
					formTypesIndexes.push_back(temp);
					pos = formTypesIndexes.size() - 1;
				}
				else
					pos = posIt - formTypesIndexes.begin();
				if (onesCounter == form_size && (find((*it).second.begin(), (*it).second.end(), pos) == (*it).second.end()))
					(*it).second.push_back(pos);
			}
		}
		it++;
	}

	cout << "Form types were saved for each combination" << endl;
}

void GetUniqueFormTypes(map<unsigned short, int> &allFormTypes)
{
	//Get all unique form types and a count of how many combinations they appear on
	map<string, vector<unsigned short>>::iterator it = combinations.begin();
	it = combinations.begin();
	map<unsigned short, int>::iterator it2;
	while (it != combinations.end())
	{
		for (int i = 0; i < (*it).second.size(); i++)
		{
			it2 = allFormTypes.find((*it).second[i]);
			if (it2 == allFormTypes.end())
			{
				pair<unsigned short, int> tempPair = make_pair((*it).second[i], 1);
				allFormTypes.insert(tempPair);
			}
			else
				(*it2).second++;
		}
		it++;
	}

	cout << "All form types with their frequencies were saved" << endl;
}

void ReverseFormTypes(map<unsigned short, int> &allFormTypes, multimap<int, unsigned short> &allFormTypesReverse)
{
	//Reverse allFormTypes in order to sort the form types according to their appearance
	map<unsigned short, int>::iterator it2;
	it2 = allFormTypes.begin();
	while (it2 != allFormTypes.end())
	{
		pair<int, unsigned short> tempPair = make_pair((*it2).second, (*it2).first);
		allFormTypesReverse.insert(tempPair);
		it2++;
	}

	//cout << "All form types list was saved as map with their frequencies as key" << endl;
}

void calcFrequencies(multimap<int, unsigned short> &allFormTypesReverse, map<string, vector<unsigned short>> &combinationsToDelete, int index)
{
	multimap<int, unsigned short> newAllFormTypesReverse;
	map<string, vector<unsigned short>>::iterator it, iter;
	map<unsigned short, int> newAllFormTypes;
	map<unsigned short, int>::iterator iter2;
	multimap<int, unsigned short>::reverse_iterator it4;
	multimap<int, unsigned short>::reverse_iterator it3;
	newAllFormTypesReverse.clear();
	it4 = allFormTypesReverse.rbegin();
	unsigned int i;
	it3 = allFormTypesReverse.rbegin();
	for (i = 0; i < index; i++)
	{
		pair<unsigned short, int> tempPair = make_pair(it3->second, it3->first);
		newAllFormTypes.insert(tempPair);
		it3++;
	}
	while (it3 != allFormTypesReverse.rend())
	{
		pair<unsigned short, int> tempPair = make_pair(it3->second, it3->first);
		iter2 = newAllFormTypes.insert(newAllFormTypes.begin(), tempPair);
		iter2->second = 0;
		it = combinationsToDelete.begin();
		while (it != combinationsToDelete.end())
		{
			iter = combinations.find(it->first);
			for (int i = 0; i < iter->second.size(); i++)
			{
				if ((iter->second)[i] == iter2->first)
				{
					iter2->second++;
					break;
				}
			}
			it++;
		}
		it3++;
	}
	ReverseFormTypes(newAllFormTypes, newAllFormTypesReverse);
	allFormTypesReverse = newAllFormTypesReverse;
}

bool FindFrequentFormTypes(vector<unsigned short> &formTypes, multimap<int, unsigned short> &allFormTypesReverse, int start, int cont)
{
	map<string, vector<unsigned short>> combinationsToDelete;
	/*Go through the remaining form types and for every combination, ignore the least frequent form types and add
	the most frequent form type to the optimized form type vector*/
	map<string, vector<unsigned short>>::iterator it, it2;
	string combToDelete;
	map<int, unsigned short>::reverse_iterator it3, it4;
	int i;

	combinationsToDelete = combinationsWithoutForms;

	it3 = allFormTypesReverse.rbegin();
	it4 = allFormTypesReverse.rbegin();

	for (i = 0; i < cont; i++)
	{
		if (i < start)
			it3++;
		it4++;
	}

	it2 = combinationsToDelete.begin();
	while (it2 != combinationsToDelete.end())
	{
		it = combinations.find((*it2).first);
		if (find((*it).second.begin(), (*it).second.end(), (*it3).second) != (*it).second.end())
		{
			if (find(formTypes.begin(), formTypes.end(), (*it3).second) == (formTypes.end()))
				formTypes.push_back((*it3).second);
			combToDelete = (*it2).first;
			it2++;
			combinationsToDelete.erase(combToDelete);
		}
		else
			it2++;
		if (combinationsToDelete.size() == 0)
			break;
	}

	while (it4 != allFormTypesReverse.rend())
	{
		it = combinations.begin();
		it2 = combinationsToDelete.begin();
		while (it2 != combinationsToDelete.end())
		{
			it = combinations.find((*it2).first);
			if (find((*it).second.begin(), (*it).second.end(), (*it4).second) != (*it).second.end())
			{
				if (find(formTypes.begin(), formTypes.end(), (*it4).second) == (formTypes.end()))
					formTypes.push_back((*it4).second);
				combToDelete = (*it2).first;
				it2++;
				combinationsToDelete.erase(combToDelete);
			}
			else
				it2++;
			if (combinationsToDelete.size() == 0)
				break;
		}
		it4++;
	}

	if (combinationsToDelete.size() != 0)
		return false;
	cout << "Form types coverage was found" << endl;
	return true;
}

bool FindFrequentFormTypes2(vector<unsigned short> &formTypes, multimap<int, unsigned short> &allFormTypesReverse, int start, int cont)
{
	map<string, vector<unsigned short>> combinationsToDelete;
	/*Go through the remaining form types and for every combination, ignore the least frequent form types and add
	the most frequent form type to the optimized form type vector*/
	map<string, vector<unsigned short>>::iterator it, it2;
	string combToDelete;
	multimap<int, unsigned short>::reverse_iterator it3;
	unsigned int i;
	vector<unsigned short> allFormTypesVec;
	vector<int> allFrequenciesVec;
	multimap<int, unsigned short> tempAllFormTypesReverse = allFormTypesReverse;

	it3 = tempAllFormTypesReverse.rbegin();
	while (it3 != tempAllFormTypesReverse.rend())
	{
		allFrequenciesVec.push_back(it3->first);
		allFormTypesVec.push_back(it3->second);
		it3++;
	}

	combinationsToDelete = combinationsWithoutForms;

	it2 = combinationsToDelete.begin();
	while (it2 != combinationsToDelete.end())
	{
		it = combinations.find((*it2).first);
		if (find((*it).second.begin(), (*it).second.end(), allFormTypesVec[start]) != (*it).second.end())
		{
			if (find(formTypes.begin(), formTypes.end(), allFormTypesVec[start]) == (formTypes.end()))
				formTypes.push_back(allFormTypesVec[start]);
			combToDelete = (*it2).first;
			it2++;
			combinationsToDelete.erase(combToDelete);
		}
		else
			it2++;
		if (combinationsToDelete.size() == 0)
			break;
	}

	calcFrequencies(tempAllFormTypesReverse, combinationsToDelete, ++start);
	it3 = tempAllFormTypesReverse.rbegin();
	allFrequenciesVec.clear();
	allFormTypesVec.clear();
	while (it3 != tempAllFormTypesReverse.rend())
	{
		allFrequenciesVec.push_back(it3->first);
		allFormTypesVec.push_back(it3->second);
		it3++;
	}

	unsigned int allFormTypesReverseSize = tempAllFormTypesReverse.size();

	unsigned int index = cont;

	for (i = 0; i < allFormTypesReverseSize; i++)
	{
		it = combinations.begin();
		it2 = combinationsToDelete.begin();
		while (it2 != combinationsToDelete.end())
		{
			it = combinations.find((*it2).first);
			if (find((*it).second.begin(), (*it).second.end(), allFormTypesVec[index]) != (*it).second.end())
			{
				if (find(formTypes.begin(), formTypes.end(), allFormTypesVec[index]) == (formTypes.end()))
					formTypes.push_back(allFormTypesVec[index]);
				combToDelete = (*it2).first;
				it2++;
				combinationsToDelete.erase(combToDelete);
			}
			else
				it2++;
			if (combinationsToDelete.size() == 0)
				break;
		}
		if (combinationsToDelete.size() == 0)
			break;

		calcFrequencies(tempAllFormTypesReverse, combinationsToDelete, ++index);
		it3 = tempAllFormTypesReverse.rbegin();
		allFrequenciesVec.clear();
		allFormTypesVec.clear();
		while (it3 != tempAllFormTypesReverse.rend())
		{
			allFrequenciesVec.push_back(it3->first);
			allFormTypesVec.push_back(it3->second);
			it3++;
		}
	}

	if (combinationsToDelete.size() != 0)
		return false;
	cout << "Form types coverage was found" << endl;
	return true;
}

bool CheckRedundantForms(vector<unsigned short> &formTypes)
{
	/*Second optimization: Go through each form type in the formTypes vector and check if there are
	redundant form types there.*/
	string combToDelete;
	map<string, vector<unsigned short>>::iterator it, it2;
	map<string, vector<unsigned short>> combinationsToDelete;
	vector<unsigned short> formTypesTest;
	bool optimized = false;
	int indexToIgnore = 0;
	while (!optimized)
	{
		for (int i = 0; i < formTypes.size(); i++)
		{
			if (indexToIgnore != i)
				formTypesTest.push_back(formTypes[i]);
		}

		combinationsToDelete = combinationsWithoutForms;
		for (int j = 0; j < formTypesTest.size(); j++)
		{
			it = combinations.begin();
			it2 = combinationsToDelete.begin();
			while (it2 != combinationsToDelete.end())
			{
				it = combinations.find((*it2).first);
				if (find((*it).second.begin(), (*it).second.end(), formTypesTest[j]) != (*it).second.end())
				{
					if (find(formTypes.begin(), formTypes.end(), formTypesTest[j]) == (formTypes.end()))
						formTypes.push_back(formTypesTest[j]);
					combToDelete = (*it2).first;
					it2++;
					combinationsToDelete.erase(combToDelete);
				}
				else
					it2++;
				if (combinationsToDelete.size() == 0)
					break;
			}
		}
		if (combinationsToDelete.size() == 0)
		{
			formTypes = formTypesTest;
			if (indexToIgnore == formTypes.size())
				optimized = true;
		}
		else
			indexToIgnore++;
		formTypesTest.clear();
		if (indexToIgnore >= formTypes.size())
			optimized = true;
	}
	cout << "Redundant forms were removed (if any)" << endl;
	if (optimized)
		return true;
	return false;
}

void CalculateFormTypes(vector<unsigned short> &formTypes)
{
	/*Optimization: Get all the combinations, for each combination get all the form types, for each form type there will be a counter of
	how many times it appears in the combinations. Then, we will go from the most frequent to the least frequent form type, we will search the
	form type in each combination and delete the rest of the form types from that combination, in the end there should be a list of
	combinations with only one form type, we will take the remaining form types and put it in the formTypes vector.*/
	formTypes.clear();
	map<unsigned short, int> allFormTypes;
	multimap<int, unsigned short> allFormTypesReverse;

	GetCombinations();

	CountFormsCombination();

	GetUniqueFormTypes(allFormTypes);

	ReverseFormTypes(allFormTypes, allFormTypesReverse);

	int min_n;

	//Save form types result to a file
	stringstream filename;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	map<int, unsigned short>::reverse_iterator it3, it4;
	int i, k;
	for (int j = 0; j < allFormTypesReverse.size(); j++)
	{
		it3 = allFormTypesReverse.rbegin();
		it4 = allFormTypesReverse.rbegin();
		it4++;
		for (i = 0; i < j; i++)
		{
			if (it3 == allFormTypesReverse.rend() || it4 == allFormTypesReverse.rend())
				break;
			it3++;
			it4++;
		}
		k = j + 1;
		while (it4 != allFormTypesReverse.rend())
		{
			vector<unsigned short> formTypesPossibility;
			if (!FindFrequentFormTypes2(formTypesPossibility, allFormTypesReverse, j, k))
				break;

			CheckRedundantForms(formTypesPossibility);

			it4++;

			mutexForms.lock();
			cout << "Number of forms : " << allFormTypesReverse.size() << endl;
			cout << "Current first and second indexes: " << j << " " << k << endl;
			for (i = 0; i < formTypesPossibility.size(); i++)
				cout << formTypesIndexes[formTypesPossibility[i]] << endl;

			//Get the set of form types with the highest effectiveness (less n is better)
			int n = 0;
			for (i = 0; i < formTypesPossibility.size(); i++)
				n = n + (20 - formTypesIndexes[formTypesPossibility[i]].size() + 1);
			cout << "Form types effectiveness: " << n << endl << endl;

			if (formTypes.empty() || n < min_n)
			{
				formTypes = formTypesPossibility;
				min_n = n;
				ofstream myfile;
				myfile.open(filename.str());
				for (i = 0; i < formTypes.size(); i++)
					myfile << formTypesIndexes[formTypes[i]] << endl;
				myfile.close();
			}
			mutexForms.unlock();
			k++;
		}
	}

	combinations.clear();
}

void CountFormsCombination2(int start)
{
	//For each combination make a vector of form types
	map<string, vector<unsigned short>>::iterator it = combinations.begin();
	vector<string>::iterator posIt;
	unsigned short pos;
	string temp;
	int i;
	for (i = 0; i < start; i++)
		it++;
	//If form size is 4
	if (form_size == 4)
	{
		while (it != combinations.end())
		{
			for (int first = 0; first < (seqSize - form_size + 1); first++)
			{
				if ((*it).first[first] == '1')
				{
					for (int second = (first + 1); second < (seqSize - form_size + 2); second++)
					{
						if ((*it).first[second] == '1')
						{
							for (int third = (second + 1); third < (seqSize - form_size + 3); third++)
							{
								if ((*it).first[third] == '1')
								{
									for (int fourth = (third + 1); fourth < (seqSize - form_size + 4); fourth++)
									{
										if ((*it).first[fourth] == '1')
										{
											temp = "1";
											for (i = first + 1; i < second; i++)
												temp += "0";
											temp += "1";
											for (i = second + 1; i < third; i++)
												temp += "0";
											temp += "1";
											for (i = third + 1; i < fourth; i++)
												temp += "0";
											temp += "1";
											mutexForms.lock();
											posIt = find(formTypesIndexes.begin(), formTypesIndexes.end(), temp);
											if (posIt == formTypesIndexes.end())
											{
												formTypesIndexes.push_back(temp);
												pos = formTypesIndexes.size() - 1;
											}
											else
												pos = posIt - formTypesIndexes.begin();
											if (find((*it).second.begin(), (*it).second.end(), pos) == (*it).second.end())
												(*it).second.push_back(pos);
											mutexForms.unlock();
										}
									}
								}
							}
						}
					}
				}
			}
			for (i = 0; i < numberOfThreads; i++)
			{
				if (it != combinations.end())
					it++;
				else
					return;
			}
		}
	}

	//If form size is 5
	if (form_size == 5)
	{
		while (it != combinations.end())
		{
			for (int first = 0; first < (seqSize - form_size + 1); first++)
			{
				if ((*it).first[first] == '1')
				{
					for (int second = (first + 1); second < (seqSize - form_size + 2); second++)
					{
						if ((*it).first[second] == '1')
						{
							for (int third = (second + 1); third < (seqSize - form_size + 3); third++)
							{
								if ((*it).first[third] == '1')
								{
									for (int fourth = (third + 1); fourth < (seqSize - form_size + 4); fourth++)
									{
										if ((*it).first[fourth] == '1')
										{
											for (int fifth = (fourth + 1); fifth < (seqSize - form_size + 5); fifth++)
											{
												if ((*it).first[fifth] == '1')
												{
													temp = "1";
													for (i = first + 1; i < second; i++)
														temp += "0";
													temp += "1";
													for (i = second + 1; i < third; i++)
														temp += "0";
													temp += "1";
													for (i = third + 1; i < fourth; i++)
														temp += "0";
													temp += "1";
													for (i = fourth + 1; i < fifth; i++)
														temp += "0";
													temp += "1";
													mutexForms.lock();
													posIt = find(formTypesIndexes.begin(), formTypesIndexes.end(), temp);
													if (posIt == formTypesIndexes.end())
													{
														formTypesIndexes.push_back(temp);
														pos = formTypesIndexes.size() - 1;
													}
													else
														pos = posIt - formTypesIndexes.begin();
													if (find((*it).second.begin(), (*it).second.end(), pos) == (*it).second.end())
														(*it).second.push_back(pos);
													mutexForms.unlock();
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			for (i = 0; i < numberOfThreads; i++)
			{
				if (it != combinations.end())
					it++;
				else
					return;
			}
		}
	}

	cout << "Form types were saved for each combination" << endl;
}

void CalculateFormTypes2(vector<unsigned short> &formTypes)
{
	/*Optimization: Get all the combinations, for each combination get all the form types (Including the cases where you ignore 1's), for each form type there will be a counter of
	how many times it appears in the combinations. Then, we will go from the most frequent to the least frequent form type, we will search the
	form type in each combination and delete the rest of the form types from that combination, in the end there should be a list of
	combinations with only one form type, we will take the remaining form types and put it in the formTypes vector.*/
	formTypes.clear();
	map<unsigned short, int> allFormTypes;
	multimap<int, unsigned short> allFormTypesReverse;
	int b;

	//Get all combinations and for each combination its form types
	GetCombinations();
	vector<thread> threadsCalc;
	for (b = 0; b < numberOfThreads; b++)
		threadsCalc.push_back(thread(CountFormsCombination2, b));
	for (b = 0; b < numberOfThreads; b++)
		threadsCalc[b].join();

	GetUniqueFormTypes(allFormTypes);

	ReverseFormTypes(allFormTypes, allFormTypesReverse);

	int min_n;

	//Save form types result to a file
	stringstream filename;
	filename << seqSize << "_" << form_size << "_" << mismatches << ".txt";
	map<int, unsigned short>::reverse_iterator it3, it4;
	int i, k;
	for (int j = 0; j < allFormTypesReverse.size(); j++)
	{
		it3 = allFormTypesReverse.rbegin();
		it4 = allFormTypesReverse.rbegin();
		it4++;
		for (i = 0; i < j; i++)
		{
			if (it3 == allFormTypesReverse.rend() || it4 == allFormTypesReverse.rend())
				break;
			it3++;
			it4++;
		}
		k = j + 1;
		while (it4 != allFormTypesReverse.rend())
		{
			vector<unsigned short> formTypesPossibility;
			if (!FindFrequentFormTypes2(formTypesPossibility, allFormTypesReverse, j, k))
				break;

			CheckRedundantForms(formTypesPossibility);

			it4++;

			mutexForms.lock();
			cout << "Number of forms : " << allFormTypesReverse.size() << endl;
			cout << "Current first and second indexes: " << j << " " << k << endl;
			for (i = 0; i < formTypesPossibility.size(); i++)
				cout << formTypesIndexes[formTypesPossibility[i]] << endl;

			//Get the set of form types with the highest effectiveness (less n is better)
			int n = 0;
			for (i = 0; i < formTypesPossibility.size(); i++)
				n = n + (20 - formTypesIndexes[formTypesPossibility[i]].size() + 1);
			cout << "Form types effectiveness: " << n << endl << endl;

			if (formTypes.empty() || n < min_n)
			{
				formTypes = formTypesPossibility;
				min_n = n;
				ofstream myfile;
				myfile.open(filename.str());
				for (i = 0; i < formTypes.size(); i++)
					myfile << formTypesIndexes[formTypes[i]] << endl;
				myfile.close();
			}
			mutexForms.unlock();
			k++;
		}
	}

	combinations.clear();
}

/*Usage: On the command line, run the exe file like this:
file.exe [mismatches] [form_size], for example: file.exe 8 4*/
int main(int argc, char *argv[])
{
	vector<unsigned short> formTypes;
	seqSize = 20;

	if (argc <= 1)
	{
		cout << "For help use -help when running the application:\nfile.exe -help" << endl;
		return 0;
	}
	/*if (argc <= 1)
	{
		mismatches = 8;
		form_size = 4;
	}*/
	else if (argc >= 2 && (argv[1])[0] == '-' && (argv[1])[1] == 'h' && (argv[1])[2] == 'e' && (argv[1])[3] == 'l' && (argv[1])[4] == 'p')
	{
		cout << "On the command line, run the exe file like this:\nfile.exe [mismatches] [form_size], for example: file.exe 8 4" << endl;
		return 0;
	}
	else if (argc == 3)
	{
		mismatches = atoi(argv[1]);
		form_size = atoi(argv[2]);
	}
	else
	{
		cout << "Invalid arguments" << endl;
		return 1;
	}

	//Get form types
	CalculateFormTypes2(formTypes);

	cout << "Finished" << endl;

	if (argc <= 1)
		cin.get();
	return 0;
}